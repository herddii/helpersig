<?php

namespace App\Models\Standard;

use Illuminate\Database\Eloquent\Model;

class Samplehasilcert extends Model
{
    // protected $connection="mysql2";
    protected $table="sample_hasil_cert";
    protected $primaryKey="id_sample_hasil";
    public $timestamps = false;
    //
}
