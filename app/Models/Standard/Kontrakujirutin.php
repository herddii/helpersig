<?php

namespace App\Models\Standard;

use Illuminate\Database\Eloquent\Model;

class Kontrakujirutin extends Model
{
    //
    // protected $connection="mysql2";
    protected $table="kont_uji_rutin";
    protected $primaryKey="id_kont_uji_rutin";
    public $timestamps = false;
}
