<?php

namespace App\Http\Controllers\Standart;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Standard\Samplehasilcert;
use App\Models\Standard\Samplehasil;
use App\Models\Standard\Kontrakujirutin;

class StandartController extends Controller
{
    public function index(Request $request){
        if (Auth::check()) {
            return view('pages.standard');            
        } else {
            return redirect('login');
        }
    }

    public function samplehasil(Request $request){
        if($request->ajax()){
            $var = \DB::table('sample_hasil as a')
            ->selectRaw('a.id_standart, a.id_paket, a.idasc, a.id_sample_hasil, a.id_kont_uji_rutin, b.nama_parameteruji')
            ->join('pmparameteruji as b','b.id_pmparameteruji','a.id_pmparameteruji')
            ->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'));
            if($request->input('id_paket')){
                $var = $var->where('a.id_paket',$request->input('id_paket'));
            }
            $var = $var->orderBy('a.id_pmparameteruji','ASC')->get();
            return datatables()->of($var)->toJson();
        }
    }

    public function samplehasilcert(Request $request){
        if($request->ajax()){
            $var = \DB::table('sample_hasil_cert as a')->selectRaw('a.id_standart, a.id_kont_uji_rutin, a.idasc, a.id_sample_hasil, a.id_pmparameteruji')
            ->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'));
            if($request->input('id_paket')){
                $var = $var->where('a.id_paket',$request->input('id_paket'));
            }
            $var = $var->orderBy('a.id_pmparameteruji','ASC')
            ->get();
            return datatables()->of($var)->toJson();
        }
    }
    public function sethelper(Request $request){
        $samplehasil = \DB::table('sample_hasil as a')
        ->selectRaw('*')
        ->where('a.id_kont_uji_rutin',$request->input('data'))->orderBy('a.id_pmparameteruji','ASC')
        ->get();

        $samplehasilcert = \DB::table('sample_hasil_cert as a')->selectRaw('*')
        ->where('a.id_kont_uji_rutin',$request->input('data'))->orderBy('a.id_pmparameteruji','ASC')
        ->get();

        foreach ($samplehasilcert as $key => $value) {            
                $helpery_shc = $value->id_kont_uji_rutin.''.$value->id_identifikasi_rutin.''.$value->id_pmparameteruji;
                $shc = Samplehasilcert::find($value->id_sample_hasil);
                $shc->id_standart = $samplehasil[$key]->id_standart;
                $shc->helper_id = round($helpery_shc);
                $shc->save();
                $sh = Samplehasil::find($samplehasil[$key]->id_sample_hasil);
                $sh->helper_id = round($helpery_shc);
                $sh->save();
        }

        return 'asd';

        // return $gabung = \DB::table('sample_hasil as a')
        // ->selectRaw('a.id_kont_uji_rutin, 
        // a.id_identifikasi_rutin, a.id_pmparameteruji, 
        // a.helper_id as helper_id_sh, 
        // a.id_sample_hasil as idsh, 
        // a.id_standart as idstandart_sh, 
        // b.id_standart as idstandart_shc, 
        // b.id_kont_uji_rutin, 
        // b.id_identifikasi_rutin, 
        // b.id_pmparameteruji, 
        // b.helper_id as helper_id_shc, 
        // b.id_sample_hasil as idshc ')
        // ->leftJoin('sample_hasil_cert as b','b.helper_id','a.helper_id')
        // ->where('a.id_kont_uji_rutin',$request->input('data'))->groupBy('a.helper_id')->get();

        // foreach ($gabung as $k) {
        //     if($k->helper_id_sh === $k->helper_id_shc){
        //         $sha = Samplehasilcert::find($k->idshc);
        //         $sha->id_standart = $k->idstandart_sh;
        //         $sha->save();
        //     }
        // }

        return $gabung;
    }

    public function nosig(Request $request){
        $var = \DB::table('kont_uji_rutin as a')
        ->selectRaw('a.id_kont_uji_rutin as id, UPPER(a.nosig) as text, id_kendali');
        if($request->has('q')){
            $var = $var->where(DB::raw('UPPER(a.nosig)'),'like','%'.$request->input('q').'%');
        }
        $var = $var->paginate(10);
        return $var;
    }

    public function kendalisig(Request $request){
        if($request->input('status') == 'get'){
            $var = \DB::table('kont_uji_rutin as a')
            ->selectRaw('a.id_kont_uji_rutin as id, UPPER(a.nosig) as text, id_kendali')
            ->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'))->get();
            return $var;
        } else {
            $kendali = Kontrakujirutin::find($request->input('id_kont_uji'));
            $kendali->id_kendali = $request->input('idkendali');
            $kendali->save();
            return 'success'; 
        }
        
    }

    public function idpaket(Request $request){
        $var = \DB::table('sample_hasil as a')
        ->selectRaw('a.id_pmparameteruji as id, b.nama_parameteruji as text')
        ->join('pmparameteruji as b','b.id_pmparameteruji','a.id_pmparameteruji');
        if($request->has('q')){
            $var = $var->where(DB::raw('UPPER(b.nama_parameteruji)'),'like','%'.$request->input('q').'%');
        }
        if($request->has('id_kont_uji')){
            $var = $var->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'));
        }
        $var = $var->groupBy('a.id_pmparameteruji')->paginate(10);
        return $var;
    }

    public function save_standart(Request $request){
        $v = $request->input('data');
        foreach ($v as $key) {
            $smplehasilcert = Samplehasilcert::find($key['id_sample']);
            $smplehasilcert->id_standart = $key['id_standart'];
            $smplehasilcert->idasc = $key['idasc'];
            $smplehasilcert->save();
        }
        return 'success bos';
    }
}
