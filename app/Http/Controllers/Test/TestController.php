<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Test\Identifikasirutin;

class TestController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            return $var = 'daw';
            // $var = Identifikasirutin::all();
            // return datatables()->of($var)->toJson();
        }
        return view('pages.testing');
    }
}
