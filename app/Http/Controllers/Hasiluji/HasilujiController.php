<?php

namespace App\Http\Controllers\Hasiluji;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Standard\Samplehasil;

class HasilujiController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            $var = \DB::table('sample_hasil as a')
            ->selectRaw('a.id_sample_hasil, b.nama_parameteruji, c.no_iden_sam, d.nosig, a.hasil_uji, a.hasil_ujii, a.hasil_ujiwb')
            ->join('pmparameteruji as b','a.id_pmparameteruji','b.id_pmparameteruji')
            ->join('identifikasi_rutin as c','a.id_identifikasi_rutin','c.id_identifikasi_rutin')
            ->join('kont_uji_rutin as d','a.id_kont_uji_rutin','d.id_kont_uji_rutin')
            ->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'));
            if($request->input('nosample')){
                $var = $var->where('a.id_identifikasi_rutin',$request->input('nosample'));
            }
            if($request->input('idpaket')){
                $var = $var->where('a.id_pmparameteruji',$request->input('idpaket'));
            }
            $var = $var->orderBy('a.id_pmparameteruji','ASC')->get();
            return datatables()->of($var)->toJson();
        }
        return view('pages.hasiluji');
    }

    public function nosample(Request $request){
        $var = \DB::table('sample_hasil as a')
            ->selectRaw('a.id_identifikasi_rutin as id, UPPER(c.no_iden_sam) as text')
            ->join('pmparameteruji as b','a.id_pmparameteruji','b.id_pmparameteruji')
            ->join('identifikasi_rutin as c','a.id_identifikasi_rutin','c.id_identifikasi_rutin')
            ->join('kont_uji_rutin as d','a.id_kont_uji_rutin','d.id_kont_uji_rutin');
        if($request->has('q')){
            $var = $var->where(DB::raw('UPPER(c.no_iden_sam)'),'like','%'.$request->input('q').'%');
        }
        if($request->has('id_kont_uji')){
            $var = $var->where('a.id_kont_uji_rutin',$request->input('id_kont_uji'));
        }
        $var = $var->groupBy('a.id_identifikasi_rutin')->paginate(10);
        return $var;
    }

    public function store(Request $request){
        $data = $request->input('data');
        foreach ($data as $key) {
            if($key['hasiluji'] == 'null'){
                $valuehasil = null;
            } else {
                $valuehasil = $key['hasiluji'];
            }
            $hasil = Samplehasil::find($key['id_samplehasil']);
            $hasil->hasil_uji = $valuehasil;
            $hasil->save();
        }
        return 'mantap bos';
    }
}
