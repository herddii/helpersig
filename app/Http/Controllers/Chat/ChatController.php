<?php

namespace App\Http\Controllers\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatController extends Controller
{
 public function index(Request $request){
     if($request->ajax()){
            $var = \DB::connection('mysql2')->table('hris_employee')->selectRaw('*')->orderBy('employee_name');
            if($request->has('data')){
                $var = $var->where(\DB::raw('UPPER(employee_name)'),'like','%'.$request->input('data').'%');
            }
            $var = $var->where('employee_name','<>','\N')->get();
            return $var;
     }
     return view('pages.chat');
 }
}
