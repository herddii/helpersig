<?php

use Illuminate\Database\Seeder;

class TblKotaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_kota')->delete();
        
        \DB::table('tbl_kota')->insert(array (
            0 => 
            array (
                'id_kota' => 1,
                'nama_kota' => 'CILEGON',
            ),
            1 => 
            array (
                'id_kota' => 2,
                'nama_kota' => 'LEBAK',
            ),
            2 => 
            array (
                'id_kota' => 3,
                'nama_kota' => 'PANDEGLANG',
            ),
            3 => 
            array (
                'id_kota' => 4,
                'nama_kota' => 'SERANG',
            ),
            4 => 
            array (
                'id_kota' => 5,
                'nama_kota' => 'TANGERANG',
            ),
            5 => 
            array (
                'id_kota' => 6,
                'nama_kota' => 'TANGERANG SELATAN',
            ),
            6 => 
            array (
                'id_kota' => 7,
                'nama_kota' => 'JAKARTA BARAT',
            ),
            7 => 
            array (
                'id_kota' => 8,
                'nama_kota' => 'JAKARTA PUSAT',
            ),
            8 => 
            array (
                'id_kota' => 9,
                'nama_kota' => 'JAKARTA SELATAN',
            ),
            9 => 
            array (
                'id_kota' => 10,
                'nama_kota' => 'JAKARTA TIMUR',
            ),
            10 => 
            array (
                'id_kota' => 11,
                'nama_kota' => 'JAKARTA UTARA',
            ),
            11 => 
            array (
                'id_kota' => 12,
                'nama_kota' => 'KEPULAUAN SERIBU',
            ),
            12 => 
            array (
                'id_kota' => 13,
                'nama_kota' => 'BANDUNG',
            ),
            13 => 
            array (
                'id_kota' => 14,
                'nama_kota' => 'BANDUNG BARAT',
            ),
            14 => 
            array (
                'id_kota' => 15,
                'nama_kota' => 'BANJAR',
            ),
            15 => 
            array (
                'id_kota' => 16,
                'nama_kota' => 'BEKASI',
            ),
            16 => 
            array (
                'id_kota' => 17,
                'nama_kota' => 'BOGOR',
            ),
            17 => 
            array (
                'id_kota' => 18,
                'nama_kota' => 'CIAMIS',
            ),
            18 => 
            array (
                'id_kota' => 19,
                'nama_kota' => 'CIANJUR',
            ),
            19 => 
            array (
                'id_kota' => 20,
                'nama_kota' => 'CIMAHI',
            ),
            20 => 
            array (
                'id_kota' => 21,
                'nama_kota' => 'CIREBON',
            ),
            21 => 
            array (
                'id_kota' => 22,
                'nama_kota' => 'DEPOK',
            ),
            22 => 
            array (
                'id_kota' => 23,
                'nama_kota' => 'GARUT',
            ),
            23 => 
            array (
                'id_kota' => 24,
                'nama_kota' => 'INDRAMAYU',
            ),
            24 => 
            array (
                'id_kota' => 25,
                'nama_kota' => 'KARAWANG',
            ),
            25 => 
            array (
                'id_kota' => 26,
                'nama_kota' => 'KUNINGAN',
            ),
            26 => 
            array (
                'id_kota' => 27,
                'nama_kota' => 'MAJALENGKA',
            ),
            27 => 
            array (
                'id_kota' => 28,
                'nama_kota' => 'PANGANDARAN',
            ),
            28 => 
            array (
                'id_kota' => 29,
                'nama_kota' => 'PURWAKARTA',
            ),
            29 => 
            array (
                'id_kota' => 30,
                'nama_kota' => 'SUBANG',
            ),
            30 => 
            array (
                'id_kota' => 31,
                'nama_kota' => 'SUKABUMI',
            ),
            31 => 
            array (
                'id_kota' => 32,
                'nama_kota' => 'SUMEDANG',
            ),
            32 => 
            array (
                'id_kota' => 33,
                'nama_kota' => 'TASIKMALAYA',
            ),
            33 => 
            array (
                'id_kota' => 34,
                'nama_kota' => 'BANJARNEGARA',
            ),
            34 => 
            array (
                'id_kota' => 35,
                'nama_kota' => 'BANYUMAS',
            ),
            35 => 
            array (
                'id_kota' => 36,
                'nama_kota' => 'BATANG',
            ),
            36 => 
            array (
                'id_kota' => 37,
                'nama_kota' => 'BLORA',
            ),
            37 => 
            array (
                'id_kota' => 38,
                'nama_kota' => 'BOYOLALI',
            ),
            38 => 
            array (
                'id_kota' => 39,
                'nama_kota' => 'BREBES',
            ),
            39 => 
            array (
                'id_kota' => 40,
                'nama_kota' => 'CILACAP',
            ),
            40 => 
            array (
                'id_kota' => 41,
                'nama_kota' => 'DEMAK',
            ),
            41 => 
            array (
                'id_kota' => 42,
                'nama_kota' => 'GROBOGAN',
            ),
            42 => 
            array (
                'id_kota' => 43,
                'nama_kota' => 'JEPARA',
            ),
            43 => 
            array (
                'id_kota' => 44,
                'nama_kota' => 'KARANGANYAR',
            ),
            44 => 
            array (
                'id_kota' => 45,
                'nama_kota' => 'KEBUMEN',
            ),
            45 => 
            array (
                'id_kota' => 46,
                'nama_kota' => 'KENDAL',
            ),
            46 => 
            array (
                'id_kota' => 47,
                'nama_kota' => 'KLATEN',
            ),
            47 => 
            array (
                'id_kota' => 48,
                'nama_kota' => 'KUDUS',
            ),
            48 => 
            array (
                'id_kota' => 49,
                'nama_kota' => 'MAGELANG',
            ),
            49 => 
            array (
                'id_kota' => 50,
                'nama_kota' => 'PATI',
            ),
            50 => 
            array (
                'id_kota' => 51,
                'nama_kota' => 'PEKALONGAN',
            ),
            51 => 
            array (
                'id_kota' => 52,
                'nama_kota' => 'PEMALANG',
            ),
            52 => 
            array (
                'id_kota' => 53,
                'nama_kota' => 'PURBALINGGA',
            ),
            53 => 
            array (
                'id_kota' => 54,
                'nama_kota' => 'PURWOREJO',
            ),
            54 => 
            array (
                'id_kota' => 55,
                'nama_kota' => 'REMBANG',
            ),
            55 => 
            array (
                'id_kota' => 56,
                'nama_kota' => 'SALATIGA',
            ),
            56 => 
            array (
                'id_kota' => 57,
                'nama_kota' => 'SEMARANG',
            ),
            57 => 
            array (
                'id_kota' => 58,
                'nama_kota' => 'SRAGEN',
            ),
            58 => 
            array (
                'id_kota' => 59,
                'nama_kota' => 'SUKOHARJO',
            ),
            59 => 
            array (
                'id_kota' => 60,
            'nama_kota' => 'SURAKARTA (SOLO)',
            ),
            60 => 
            array (
                'id_kota' => 61,
                'nama_kota' => 'TEGAL',
            ),
            61 => 
            array (
                'id_kota' => 62,
                'nama_kota' => 'TEMANGGUNG',
            ),
            62 => 
            array (
                'id_kota' => 63,
                'nama_kota' => 'WONOGIRI',
            ),
            63 => 
            array (
                'id_kota' => 64,
                'nama_kota' => 'WONOSOBO',
            ),
            64 => 
            array (
                'id_kota' => 65,
                'nama_kota' => 'BANTUL',
            ),
            65 => 
            array (
                'id_kota' => 66,
                'nama_kota' => 'GUNUNG KIDUL',
            ),
            66 => 
            array (
                'id_kota' => 67,
                'nama_kota' => 'KULON PROGO',
            ),
            67 => 
            array (
                'id_kota' => 68,
                'nama_kota' => 'SLEMAN',
            ),
            68 => 
            array (
                'id_kota' => 69,
                'nama_kota' => 'YOGYAKARTA',
            ),
            69 => 
            array (
                'id_kota' => 70,
                'nama_kota' => 'BANGKALAN',
            ),
            70 => 
            array (
                'id_kota' => 71,
                'nama_kota' => 'BANYUWANGI',
            ),
            71 => 
            array (
                'id_kota' => 72,
                'nama_kota' => 'BATU',
            ),
            72 => 
            array (
                'id_kota' => 73,
                'nama_kota' => 'BLITAR',
            ),
            73 => 
            array (
                'id_kota' => 74,
                'nama_kota' => 'BOJONEGORO',
            ),
            74 => 
            array (
                'id_kota' => 75,
                'nama_kota' => 'BONDOWOSO',
            ),
            75 => 
            array (
                'id_kota' => 76,
                'nama_kota' => 'GRESIK',
            ),
            76 => 
            array (
                'id_kota' => 77,
                'nama_kota' => 'JEMBER',
            ),
            77 => 
            array (
                'id_kota' => 78,
                'nama_kota' => 'JOMBANG',
            ),
            78 => 
            array (
                'id_kota' => 79,
                'nama_kota' => 'KEDIRI',
            ),
            79 => 
            array (
                'id_kota' => 80,
                'nama_kota' => 'LAMONGAN',
            ),
            80 => 
            array (
                'id_kota' => 81,
                'nama_kota' => 'LUMAJANG',
            ),
            81 => 
            array (
                'id_kota' => 82,
                'nama_kota' => 'MADIUN',
            ),
            82 => 
            array (
                'id_kota' => 83,
                'nama_kota' => 'MAGETAN',
            ),
            83 => 
            array (
                'id_kota' => 84,
                'nama_kota' => 'MALANG',
            ),
            84 => 
            array (
                'id_kota' => 85,
                'nama_kota' => 'MOJOKERTO',
            ),
            85 => 
            array (
                'id_kota' => 86,
                'nama_kota' => 'NGANJUK',
            ),
            86 => 
            array (
                'id_kota' => 87,
                'nama_kota' => 'NGAWI',
            ),
            87 => 
            array (
                'id_kota' => 88,
                'nama_kota' => 'PACITAN',
            ),
            88 => 
            array (
                'id_kota' => 89,
                'nama_kota' => 'PAMEKASAN',
            ),
            89 => 
            array (
                'id_kota' => 90,
                'nama_kota' => 'PASURUAN',
            ),
            90 => 
            array (
                'id_kota' => 91,
                'nama_kota' => 'PONOROGO',
            ),
            91 => 
            array (
                'id_kota' => 92,
                'nama_kota' => 'PROBOLINGGO',
            ),
            92 => 
            array (
                'id_kota' => 93,
                'nama_kota' => 'SAMPANG',
            ),
            93 => 
            array (
                'id_kota' => 94,
                'nama_kota' => 'SIDOARJO',
            ),
            94 => 
            array (
                'id_kota' => 95,
                'nama_kota' => 'SITUBONDO',
            ),
            95 => 
            array (
                'id_kota' => 96,
                'nama_kota' => 'SUMENEP',
            ),
            96 => 
            array (
                'id_kota' => 97,
                'nama_kota' => 'SURABAYA',
            ),
            97 => 
            array (
                'id_kota' => 98,
                'nama_kota' => 'TRENGGALEK',
            ),
            98 => 
            array (
                'id_kota' => 99,
                'nama_kota' => 'TUBAN',
            ),
            99 => 
            array (
                'id_kota' => 100,
                'nama_kota' => 'TULUNGAGUNG',
            ),
            100 => 
            array (
                'id_kota' => 101,
                'nama_kota' => 'BADUNG',
            ),
            101 => 
            array (
                'id_kota' => 102,
                'nama_kota' => 'BANGLI',
            ),
            102 => 
            array (
                'id_kota' => 103,
                'nama_kota' => 'BULELENG',
            ),
            103 => 
            array (
                'id_kota' => 104,
                'nama_kota' => 'DENPASAR',
            ),
            104 => 
            array (
                'id_kota' => 105,
                'nama_kota' => 'GIANYAR',
            ),
            105 => 
            array (
                'id_kota' => 106,
                'nama_kota' => 'JEMBRANA',
            ),
            106 => 
            array (
                'id_kota' => 107,
                'nama_kota' => 'KARANGASEM',
            ),
            107 => 
            array (
                'id_kota' => 108,
                'nama_kota' => 'KLUNGKUNG',
            ),
            108 => 
            array (
                'id_kota' => 109,
                'nama_kota' => 'TABANAN',
            ),
            109 => 
            array (
                'id_kota' => 110,
                'nama_kota' => 'ACEH BARAT',
            ),
            110 => 
            array (
                'id_kota' => 111,
                'nama_kota' => 'ACEH BARAT DAYA',
            ),
            111 => 
            array (
                'id_kota' => 112,
                'nama_kota' => 'ACEH BESAR',
            ),
            112 => 
            array (
                'id_kota' => 113,
                'nama_kota' => 'ACEH JAYA',
            ),
            113 => 
            array (
                'id_kota' => 114,
                'nama_kota' => 'ACEH SELATAN',
            ),
            114 => 
            array (
                'id_kota' => 115,
                'nama_kota' => 'ACEH SINGKIL',
            ),
            115 => 
            array (
                'id_kota' => 116,
                'nama_kota' => 'ACEH TAMIANG',
            ),
            116 => 
            array (
                'id_kota' => 117,
                'nama_kota' => 'ACEH TENGAH',
            ),
            117 => 
            array (
                'id_kota' => 118,
                'nama_kota' => 'ACEH TENGGARA',
            ),
            118 => 
            array (
                'id_kota' => 119,
                'nama_kota' => 'ACEH TIMUR',
            ),
            119 => 
            array (
                'id_kota' => 120,
                'nama_kota' => 'ACEH UTARA',
            ),
            120 => 
            array (
                'id_kota' => 121,
                'nama_kota' => 'BANDA ACEH',
            ),
            121 => 
            array (
                'id_kota' => 122,
                'nama_kota' => 'BENER MERIAH',
            ),
            122 => 
            array (
                'id_kota' => 123,
                'nama_kota' => 'BIREUEN',
            ),
            123 => 
            array (
                'id_kota' => 124,
                'nama_kota' => 'GAYO LUES',
            ),
            124 => 
            array (
                'id_kota' => 125,
                'nama_kota' => 'LANGSA',
            ),
            125 => 
            array (
                'id_kota' => 126,
                'nama_kota' => 'LHOKSEUMAWE',
            ),
            126 => 
            array (
                'id_kota' => 127,
                'nama_kota' => 'NAGAN RAYA',
            ),
            127 => 
            array (
                'id_kota' => 128,
                'nama_kota' => 'PIDIE',
            ),
            128 => 
            array (
                'id_kota' => 129,
                'nama_kota' => 'PIDIE JAYA',
            ),
            129 => 
            array (
                'id_kota' => 130,
                'nama_kota' => 'SABANG',
            ),
            130 => 
            array (
                'id_kota' => 131,
                'nama_kota' => 'SIMEULUE',
            ),
            131 => 
            array (
                'id_kota' => 132,
                'nama_kota' => 'SUBULUSSALAM',
            ),
            132 => 
            array (
                'id_kota' => 133,
                'nama_kota' => 'ASAHAN',
            ),
            133 => 
            array (
                'id_kota' => 134,
                'nama_kota' => 'BATU BARA',
            ),
            134 => 
            array (
                'id_kota' => 135,
                'nama_kota' => 'BINJAI',
            ),
            135 => 
            array (
                'id_kota' => 136,
                'nama_kota' => 'DAIRI',
            ),
            136 => 
            array (
                'id_kota' => 137,
                'nama_kota' => 'DELI SERDANG',
            ),
            137 => 
            array (
                'id_kota' => 138,
                'nama_kota' => 'GUNUNGSITOLI',
            ),
            138 => 
            array (
                'id_kota' => 139,
                'nama_kota' => 'HUMBANG HASUNDUTAN',
            ),
            139 => 
            array (
                'id_kota' => 140,
                'nama_kota' => 'KARO',
            ),
            140 => 
            array (
                'id_kota' => 141,
                'nama_kota' => 'LABUHAN BATU',
            ),
            141 => 
            array (
                'id_kota' => 142,
                'nama_kota' => 'LABUHAN BATU SELATAN',
            ),
            142 => 
            array (
                'id_kota' => 143,
                'nama_kota' => 'LABUHAN BATU UTARA',
            ),
            143 => 
            array (
                'id_kota' => 144,
                'nama_kota' => 'LANGKAT',
            ),
            144 => 
            array (
                'id_kota' => 145,
                'nama_kota' => 'MANDAILING NATAL',
            ),
            145 => 
            array (
                'id_kota' => 146,
                'nama_kota' => 'MEDAN',
            ),
            146 => 
            array (
                'id_kota' => 147,
                'nama_kota' => 'NIAS',
            ),
            147 => 
            array (
                'id_kota' => 148,
                'nama_kota' => 'NIAS BARAT',
            ),
            148 => 
            array (
                'id_kota' => 149,
                'nama_kota' => 'NIAS SELATAN',
            ),
            149 => 
            array (
                'id_kota' => 150,
                'nama_kota' => 'NIAS UTARA',
            ),
            150 => 
            array (
                'id_kota' => 151,
                'nama_kota' => 'PADANG LAWAS',
            ),
            151 => 
            array (
                'id_kota' => 152,
                'nama_kota' => 'PADANG LAWAS UTARA',
            ),
            152 => 
            array (
                'id_kota' => 153,
                'nama_kota' => 'PADANG SIDEMPUAN',
            ),
            153 => 
            array (
                'id_kota' => 154,
                'nama_kota' => 'PAKPAK BHARAT',
            ),
            154 => 
            array (
                'id_kota' => 155,
                'nama_kota' => 'PEMATANG SIANTAR',
            ),
            155 => 
            array (
                'id_kota' => 156,
                'nama_kota' => 'SAMOSIR',
            ),
            156 => 
            array (
                'id_kota' => 157,
                'nama_kota' => 'SERDANG BEDAGAI',
            ),
            157 => 
            array (
                'id_kota' => 158,
                'nama_kota' => 'SIBOLGA',
            ),
            158 => 
            array (
                'id_kota' => 159,
                'nama_kota' => 'SIMALUNGUN',
            ),
            159 => 
            array (
                'id_kota' => 160,
                'nama_kota' => 'TANJUNG BALAI',
            ),
            160 => 
            array (
                'id_kota' => 161,
                'nama_kota' => 'TAPANULI SELATAN',
            ),
            161 => 
            array (
                'id_kota' => 162,
                'nama_kota' => 'TAPANULI TENGAH',
            ),
            162 => 
            array (
                'id_kota' => 163,
                'nama_kota' => 'TAPANULI UTARA',
            ),
            163 => 
            array (
                'id_kota' => 164,
                'nama_kota' => 'TEBING TINGGI',
            ),
            164 => 
            array (
                'id_kota' => 165,
                'nama_kota' => 'TOBA SAMOSIR',
            ),
            165 => 
            array (
                'id_kota' => 166,
                'nama_kota' => 'AGAM',
            ),
            166 => 
            array (
                'id_kota' => 167,
                'nama_kota' => 'BUKITTINGGI',
            ),
            167 => 
            array (
                'id_kota' => 168,
                'nama_kota' => 'DHARMASRAYA',
            ),
            168 => 
            array (
                'id_kota' => 169,
                'nama_kota' => 'KEPULAUAN MENTAWAI',
            ),
            169 => 
            array (
                'id_kota' => 170,
                'nama_kota' => 'LIMA PULUH KOTO/KOTA',
            ),
            170 => 
            array (
                'id_kota' => 171,
                'nama_kota' => 'PADANG',
            ),
            171 => 
            array (
                'id_kota' => 172,
                'nama_kota' => 'PADANG PANJANG',
            ),
            172 => 
            array (
                'id_kota' => 173,
                'nama_kota' => 'PADANG PARIAMAN',
            ),
            173 => 
            array (
                'id_kota' => 174,
                'nama_kota' => 'PARIAMAN',
            ),
            174 => 
            array (
                'id_kota' => 175,
                'nama_kota' => 'PASAMAN',
            ),
            175 => 
            array (
                'id_kota' => 176,
                'nama_kota' => 'PASAMAN BARAT',
            ),
            176 => 
            array (
                'id_kota' => 177,
                'nama_kota' => 'PAYAKUMBUH',
            ),
            177 => 
            array (
                'id_kota' => 178,
                'nama_kota' => 'PESISIR SELATAN',
            ),
            178 => 
            array (
                'id_kota' => 179,
                'nama_kota' => 'SAWAH LUNTO',
            ),
            179 => 
            array (
                'id_kota' => 180,
            'nama_kota' => 'SIJUNJUNG (SAWAH LUNTO SIJUNJUNG)',
            ),
            180 => 
            array (
                'id_kota' => 181,
                'nama_kota' => 'SOLOK',
            ),
            181 => 
            array (
                'id_kota' => 182,
                'nama_kota' => 'SOLOK SELATAN',
            ),
            182 => 
            array (
                'id_kota' => 183,
                'nama_kota' => 'TANAH DATAR',
            ),
            183 => 
            array (
                'id_kota' => 184,
                'nama_kota' => 'BENGKALIS',
            ),
            184 => 
            array (
                'id_kota' => 185,
                'nama_kota' => 'DUMAI',
            ),
            185 => 
            array (
                'id_kota' => 186,
                'nama_kota' => 'INDRAGIRI HILIR',
            ),
            186 => 
            array (
                'id_kota' => 187,
                'nama_kota' => 'INDRAGIRI HULU',
            ),
            187 => 
            array (
                'id_kota' => 188,
                'nama_kota' => 'KAMPAR',
            ),
            188 => 
            array (
                'id_kota' => 189,
                'nama_kota' => 'KEPULAUAN MERANTI',
            ),
            189 => 
            array (
                'id_kota' => 190,
                'nama_kota' => 'KUANTAN SINGINGI',
            ),
            190 => 
            array (
                'id_kota' => 191,
                'nama_kota' => 'PEKANBARU',
            ),
            191 => 
            array (
                'id_kota' => 192,
                'nama_kota' => 'PELALAWAN',
            ),
            192 => 
            array (
                'id_kota' => 193,
                'nama_kota' => 'ROKAN HILIR',
            ),
            193 => 
            array (
                'id_kota' => 194,
                'nama_kota' => 'ROKAN HULU',
            ),
            194 => 
            array (
                'id_kota' => 195,
                'nama_kota' => 'SIAK',
            ),
            195 => 
            array (
                'id_kota' => 196,
                'nama_kota' => 'BATAM',
            ),
            196 => 
            array (
                'id_kota' => 197,
                'nama_kota' => 'BINTAN',
            ),
            197 => 
            array (
                'id_kota' => 198,
                'nama_kota' => 'KARIMUN',
            ),
            198 => 
            array (
                'id_kota' => 199,
                'nama_kota' => 'KEPULAUAN ANAMBAS',
            ),
            199 => 
            array (
                'id_kota' => 200,
                'nama_kota' => 'LINGGA',
            ),
            200 => 
            array (
                'id_kota' => 201,
                'nama_kota' => 'NATUNA',
            ),
            201 => 
            array (
                'id_kota' => 202,
                'nama_kota' => 'TANJUNG PINANG',
            ),
            202 => 
            array (
                'id_kota' => 203,
                'nama_kota' => 'BATANG HARI',
            ),
            203 => 
            array (
                'id_kota' => 204,
                'nama_kota' => 'BUNGO',
            ),
            204 => 
            array (
                'id_kota' => 205,
                'nama_kota' => 'JAMBI',
            ),
            205 => 
            array (
                'id_kota' => 206,
                'nama_kota' => 'KERINCI',
            ),
            206 => 
            array (
                'id_kota' => 207,
                'nama_kota' => 'MERANGIN',
            ),
            207 => 
            array (
                'id_kota' => 208,
                'nama_kota' => 'MUARO JAMBI',
            ),
            208 => 
            array (
                'id_kota' => 209,
                'nama_kota' => 'SAROLANGUN',
            ),
            209 => 
            array (
                'id_kota' => 210,
                'nama_kota' => 'SUNGAIPENUH',
            ),
            210 => 
            array (
                'id_kota' => 211,
                'nama_kota' => 'TANJUNG JABUNG BARAT',
            ),
            211 => 
            array (
                'id_kota' => 212,
                'nama_kota' => 'TANJUNG JABUNG TIMUR',
            ),
            212 => 
            array (
                'id_kota' => 213,
                'nama_kota' => 'TEBO',
            ),
            213 => 
            array (
                'id_kota' => 214,
                'nama_kota' => 'BENGKULU',
            ),
            214 => 
            array (
                'id_kota' => 215,
                'nama_kota' => 'BENGKULU SELATAN',
            ),
            215 => 
            array (
                'id_kota' => 216,
                'nama_kota' => 'BENGKULU TENGAH',
            ),
            216 => 
            array (
                'id_kota' => 217,
                'nama_kota' => 'BENGKULU UTARA',
            ),
            217 => 
            array (
                'id_kota' => 218,
                'nama_kota' => 'KAUR',
            ),
            218 => 
            array (
                'id_kota' => 219,
                'nama_kota' => 'KEPAHIANG',
            ),
            219 => 
            array (
                'id_kota' => 220,
                'nama_kota' => 'LEBONG',
            ),
            220 => 
            array (
                'id_kota' => 221,
                'nama_kota' => 'MUKO MUKO',
            ),
            221 => 
            array (
                'id_kota' => 222,
                'nama_kota' => 'REJANG LEBONG',
            ),
            222 => 
            array (
                'id_kota' => 223,
                'nama_kota' => 'SELUMA',
            ),
            223 => 
            array (
                'id_kota' => 224,
                'nama_kota' => 'BANYUASIN',
            ),
            224 => 
            array (
                'id_kota' => 225,
                'nama_kota' => 'EMPAT LAWANG',
            ),
            225 => 
            array (
                'id_kota' => 226,
                'nama_kota' => 'LAHAT',
            ),
            226 => 
            array (
                'id_kota' => 227,
                'nama_kota' => 'LUBUK LINGGAU',
            ),
            227 => 
            array (
                'id_kota' => 228,
                'nama_kota' => 'MUARA ENIM',
            ),
            228 => 
            array (
                'id_kota' => 229,
                'nama_kota' => 'MUSI BANYUASIN',
            ),
            229 => 
            array (
                'id_kota' => 230,
                'nama_kota' => 'MUSI RAWAS',
            ),
            230 => 
            array (
                'id_kota' => 231,
                'nama_kota' => 'OGAN ILIR',
            ),
            231 => 
            array (
                'id_kota' => 232,
                'nama_kota' => 'OGAN KOMERING ILIR',
            ),
            232 => 
            array (
                'id_kota' => 233,
                'nama_kota' => 'OGAN KOMERING ULU',
            ),
            233 => 
            array (
                'id_kota' => 234,
                'nama_kota' => 'OGAN KOMERING ULU SELATAN',
            ),
            234 => 
            array (
                'id_kota' => 235,
                'nama_kota' => 'OGAN KOMERING ULU TIMUR',
            ),
            235 => 
            array (
                'id_kota' => 236,
                'nama_kota' => 'PAGAR ALAM',
            ),
            236 => 
            array (
                'id_kota' => 237,
                'nama_kota' => 'PALEMBANG',
            ),
            237 => 
            array (
                'id_kota' => 238,
                'nama_kota' => 'PRABUMULIH',
            ),
            238 => 
            array (
                'id_kota' => 239,
                'nama_kota' => 'BANGKA',
            ),
            239 => 
            array (
                'id_kota' => 240,
                'nama_kota' => 'BANGKA BARAT',
            ),
            240 => 
            array (
                'id_kota' => 241,
                'nama_kota' => 'BANGKA SELATAN',
            ),
            241 => 
            array (
                'id_kota' => 242,
                'nama_kota' => 'BANGKA TENGAH',
            ),
            242 => 
            array (
                'id_kota' => 243,
                'nama_kota' => 'BELITUNG',
            ),
            243 => 
            array (
                'id_kota' => 244,
                'nama_kota' => 'BELITUNG TIMUR',
            ),
            244 => 
            array (
                'id_kota' => 245,
                'nama_kota' => 'PANGKAL PINANG',
            ),
            245 => 
            array (
                'id_kota' => 246,
                'nama_kota' => 'BANDAR LAMPUNG',
            ),
            246 => 
            array (
                'id_kota' => 247,
                'nama_kota' => 'LAMPUNG BARAT',
            ),
            247 => 
            array (
                'id_kota' => 248,
                'nama_kota' => 'LAMPUNG SELATAN',
            ),
            248 => 
            array (
                'id_kota' => 249,
                'nama_kota' => 'LAMPUNG TENGAH',
            ),
            249 => 
            array (
                'id_kota' => 250,
                'nama_kota' => 'LAMPUNG TIMUR',
            ),
            250 => 
            array (
                'id_kota' => 251,
                'nama_kota' => 'LAMPUNG UTARA',
            ),
            251 => 
            array (
                'id_kota' => 252,
                'nama_kota' => 'MESUJI',
            ),
            252 => 
            array (
                'id_kota' => 253,
                'nama_kota' => 'METRO',
            ),
            253 => 
            array (
                'id_kota' => 254,
                'nama_kota' => 'PESAWARAN',
            ),
            254 => 
            array (
                'id_kota' => 255,
                'nama_kota' => 'PESISIR BARAT',
            ),
            255 => 
            array (
                'id_kota' => 256,
                'nama_kota' => 'PRINGSEWU',
            ),
            256 => 
            array (
                'id_kota' => 257,
                'nama_kota' => 'TANGGAMUS',
            ),
            257 => 
            array (
                'id_kota' => 258,
                'nama_kota' => 'TULANG BAWANG',
            ),
            258 => 
            array (
                'id_kota' => 259,
                'nama_kota' => 'TULANG BAWANG BARAT',
            ),
            259 => 
            array (
                'id_kota' => 260,
                'nama_kota' => 'WAY KANAN',
            ),
            260 => 
            array (
                'id_kota' => 261,
                'nama_kota' => 'BENGKAYANG',
            ),
            261 => 
            array (
                'id_kota' => 262,
                'nama_kota' => 'KAPUAS HULU',
            ),
            262 => 
            array (
                'id_kota' => 263,
                'nama_kota' => 'KAYONG UTARA',
            ),
            263 => 
            array (
                'id_kota' => 264,
                'nama_kota' => 'KETAPANG',
            ),
            264 => 
            array (
                'id_kota' => 265,
                'nama_kota' => 'KUBU RAYA',
            ),
            265 => 
            array (
                'id_kota' => 266,
                'nama_kota' => 'LANDAK',
            ),
            266 => 
            array (
                'id_kota' => 267,
                'nama_kota' => 'MELAWI',
            ),
            267 => 
            array (
                'id_kota' => 268,
                'nama_kota' => 'PONTIANAK',
            ),
            268 => 
            array (
                'id_kota' => 269,
                'nama_kota' => 'SAMBAS',
            ),
            269 => 
            array (
                'id_kota' => 270,
                'nama_kota' => 'SANGGAU',
            ),
            270 => 
            array (
                'id_kota' => 271,
                'nama_kota' => 'SEKADAU',
            ),
            271 => 
            array (
                'id_kota' => 272,
                'nama_kota' => 'SINGKAWANG',
            ),
            272 => 
            array (
                'id_kota' => 273,
                'nama_kota' => 'SINTANG',
            ),
            273 => 
            array (
                'id_kota' => 274,
                'nama_kota' => 'BARITO SELATAN',
            ),
            274 => 
            array (
                'id_kota' => 275,
                'nama_kota' => 'BARITO TIMUR',
            ),
            275 => 
            array (
                'id_kota' => 276,
                'nama_kota' => 'BARITO UTARA',
            ),
            276 => 
            array (
                'id_kota' => 277,
                'nama_kota' => 'GUNUNG MAS',
            ),
            277 => 
            array (
                'id_kota' => 278,
                'nama_kota' => 'KAPUAS',
            ),
            278 => 
            array (
                'id_kota' => 279,
                'nama_kota' => 'KATINGAN',
            ),
            279 => 
            array (
                'id_kota' => 280,
                'nama_kota' => 'KOTAWARINGIN BARAT',
            ),
            280 => 
            array (
                'id_kota' => 281,
                'nama_kota' => 'KOTAWARINGIN TIMUR',
            ),
            281 => 
            array (
                'id_kota' => 282,
                'nama_kota' => 'LAMANDAU',
            ),
            282 => 
            array (
                'id_kota' => 283,
                'nama_kota' => 'MURUNG RAYA',
            ),
            283 => 
            array (
                'id_kota' => 284,
                'nama_kota' => 'PALANGKA RAYA',
            ),
            284 => 
            array (
                'id_kota' => 285,
                'nama_kota' => 'PULANG PISAU',
            ),
            285 => 
            array (
                'id_kota' => 286,
                'nama_kota' => 'SERUYAN',
            ),
            286 => 
            array (
                'id_kota' => 287,
                'nama_kota' => 'SUKAMARA',
            ),
            287 => 
            array (
                'id_kota' => 288,
                'nama_kota' => 'BALANGAN',
            ),
            288 => 
            array (
                'id_kota' => 289,
                'nama_kota' => 'BANJAR',
            ),
            289 => 
            array (
                'id_kota' => 290,
                'nama_kota' => 'BANJARBARU',
            ),
            290 => 
            array (
                'id_kota' => 291,
                'nama_kota' => 'BANJARMASIN',
            ),
            291 => 
            array (
                'id_kota' => 292,
                'nama_kota' => 'BARITO KUALA',
            ),
            292 => 
            array (
                'id_kota' => 293,
                'nama_kota' => 'HULU SUNGAI SELATAN',
            ),
            293 => 
            array (
                'id_kota' => 294,
                'nama_kota' => 'HULU SUNGAI TENGAH',
            ),
            294 => 
            array (
                'id_kota' => 295,
                'nama_kota' => 'HULU SUNGAI UTARA',
            ),
            295 => 
            array (
                'id_kota' => 296,
                'nama_kota' => 'KOTABARU',
            ),
            296 => 
            array (
                'id_kota' => 297,
                'nama_kota' => 'TABALONG',
            ),
            297 => 
            array (
                'id_kota' => 298,
                'nama_kota' => 'TANAH BUMBU',
            ),
            298 => 
            array (
                'id_kota' => 299,
                'nama_kota' => 'TANAH LAUT',
            ),
            299 => 
            array (
                'id_kota' => 300,
                'nama_kota' => 'TAPIN',
            ),
            300 => 
            array (
                'id_kota' => 301,
                'nama_kota' => 'BALIKPAPAN',
            ),
            301 => 
            array (
                'id_kota' => 302,
                'nama_kota' => 'BERAU',
            ),
            302 => 
            array (
                'id_kota' => 303,
                'nama_kota' => 'BONTANG',
            ),
            303 => 
            array (
                'id_kota' => 304,
                'nama_kota' => 'KUTAI BARAT',
            ),
            304 => 
            array (
                'id_kota' => 305,
                'nama_kota' => 'KUTAI KARTANEGARA',
            ),
            305 => 
            array (
                'id_kota' => 306,
                'nama_kota' => 'KUTAI TIMUR',
            ),
            306 => 
            array (
                'id_kota' => 307,
                'nama_kota' => 'PASER',
            ),
            307 => 
            array (
                'id_kota' => 308,
                'nama_kota' => 'PENAJAM PASER UTARA',
            ),
            308 => 
            array (
                'id_kota' => 309,
                'nama_kota' => 'SAMARINDA',
            ),
            309 => 
            array (
                'id_kota' => 310,
            'nama_kota' => 'BULUNGAN (BULONGAN)',
            ),
            310 => 
            array (
                'id_kota' => 311,
                'nama_kota' => 'MALINAU',
            ),
            311 => 
            array (
                'id_kota' => 312,
                'nama_kota' => 'NUNUKAN',
            ),
            312 => 
            array (
                'id_kota' => 313,
                'nama_kota' => 'TANA TIDUNG',
            ),
            313 => 
            array (
                'id_kota' => 314,
                'nama_kota' => 'TARAKAN',
            ),
            314 => 
            array (
                'id_kota' => 315,
                'nama_kota' => 'MAJENE',
            ),
            315 => 
            array (
                'id_kota' => 316,
                'nama_kota' => 'MAMASA',
            ),
            316 => 
            array (
                'id_kota' => 317,
                'nama_kota' => 'MAMUJU',
            ),
            317 => 
            array (
                'id_kota' => 318,
                'nama_kota' => 'MAMUJU UTARA',
            ),
            318 => 
            array (
                'id_kota' => 319,
                'nama_kota' => 'POLEWALI MANDAR',
            ),
            319 => 
            array (
                'id_kota' => 320,
                'nama_kota' => 'BANTAENG',
            ),
            320 => 
            array (
                'id_kota' => 321,
                'nama_kota' => 'BARRU',
            ),
            321 => 
            array (
                'id_kota' => 322,
                'nama_kota' => 'BONE',
            ),
            322 => 
            array (
                'id_kota' => 323,
                'nama_kota' => 'BULUKUMBA',
            ),
            323 => 
            array (
                'id_kota' => 324,
                'nama_kota' => 'ENREKANG',
            ),
            324 => 
            array (
                'id_kota' => 325,
                'nama_kota' => 'GOWA',
            ),
            325 => 
            array (
                'id_kota' => 326,
                'nama_kota' => 'JENEPONTO',
            ),
            326 => 
            array (
                'id_kota' => 327,
                'nama_kota' => 'LUWU',
            ),
            327 => 
            array (
                'id_kota' => 328,
                'nama_kota' => 'LUWU TIMUR',
            ),
            328 => 
            array (
                'id_kota' => 329,
                'nama_kota' => 'LUWU UTARA',
            ),
            329 => 
            array (
                'id_kota' => 330,
                'nama_kota' => 'MAKASSAR',
            ),
            330 => 
            array (
                'id_kota' => 331,
                'nama_kota' => 'MAROS',
            ),
            331 => 
            array (
                'id_kota' => 332,
                'nama_kota' => 'PALOPO',
            ),
            332 => 
            array (
                'id_kota' => 333,
                'nama_kota' => 'PANGKAJENE KEPULAUAN',
            ),
            333 => 
            array (
                'id_kota' => 334,
                'nama_kota' => 'PAREPARE',
            ),
            334 => 
            array (
                'id_kota' => 335,
                'nama_kota' => 'PINRANG',
            ),
            335 => 
            array (
                'id_kota' => 336,
            'nama_kota' => 'SELAYAR (KEPULAUAN SELAYAR)',
            ),
            336 => 
            array (
                'id_kota' => 337,
                'nama_kota' => 'SIDENRENG RAPPANG/RAPANG',
            ),
            337 => 
            array (
                'id_kota' => 338,
                'nama_kota' => 'SINJAI',
            ),
            338 => 
            array (
                'id_kota' => 339,
                'nama_kota' => 'SOPPENG',
            ),
            339 => 
            array (
                'id_kota' => 340,
                'nama_kota' => 'TAKALAR',
            ),
            340 => 
            array (
                'id_kota' => 341,
                'nama_kota' => 'TANA TORAJA',
            ),
            341 => 
            array (
                'id_kota' => 342,
                'nama_kota' => 'TORAJA UTARA',
            ),
            342 => 
            array (
                'id_kota' => 343,
                'nama_kota' => 'WAJO',
            ),
            343 => 
            array (
                'id_kota' => 344,
                'nama_kota' => 'BAU-BAU',
            ),
            344 => 
            array (
                'id_kota' => 345,
                'nama_kota' => 'BOMBANA',
            ),
            345 => 
            array (
                'id_kota' => 346,
                'nama_kota' => 'BUTON',
            ),
            346 => 
            array (
                'id_kota' => 347,
                'nama_kota' => 'BUTON UTARA',
            ),
            347 => 
            array (
                'id_kota' => 348,
                'nama_kota' => 'KENDARI',
            ),
            348 => 
            array (
                'id_kota' => 349,
                'nama_kota' => 'KOLAKA',
            ),
            349 => 
            array (
                'id_kota' => 350,
                'nama_kota' => 'KOLAKA UTARA',
            ),
            350 => 
            array (
                'id_kota' => 351,
                'nama_kota' => 'KONAWE',
            ),
            351 => 
            array (
                'id_kota' => 352,
                'nama_kota' => 'KONAWE SELATAN',
            ),
            352 => 
            array (
                'id_kota' => 353,
                'nama_kota' => 'KONAWE UTARA',
            ),
            353 => 
            array (
                'id_kota' => 354,
                'nama_kota' => 'MUNA',
            ),
            354 => 
            array (
                'id_kota' => 355,
                'nama_kota' => 'WAKATOBI',
            ),
            355 => 
            array (
                'id_kota' => 356,
                'nama_kota' => 'BANGGAI',
            ),
            356 => 
            array (
                'id_kota' => 357,
                'nama_kota' => 'BANGGAI KEPULAUAN',
            ),
            357 => 
            array (
                'id_kota' => 358,
                'nama_kota' => 'BUOL',
            ),
            358 => 
            array (
                'id_kota' => 359,
                'nama_kota' => 'DONGGALA',
            ),
            359 => 
            array (
                'id_kota' => 360,
                'nama_kota' => 'MOROWALI',
            ),
            360 => 
            array (
                'id_kota' => 361,
                'nama_kota' => 'PALU',
            ),
            361 => 
            array (
                'id_kota' => 362,
                'nama_kota' => 'PARIGI MOUTONG',
            ),
            362 => 
            array (
                'id_kota' => 363,
                'nama_kota' => 'POSO',
            ),
            363 => 
            array (
                'id_kota' => 364,
                'nama_kota' => 'SIGI',
            ),
            364 => 
            array (
                'id_kota' => 365,
                'nama_kota' => 'TOJO UNA-UNA',
            ),
            365 => 
            array (
                'id_kota' => 366,
                'nama_kota' => 'TOLI-TOLI',
            ),
            366 => 
            array (
                'id_kota' => 367,
                'nama_kota' => 'BOALEMO',
            ),
            367 => 
            array (
                'id_kota' => 368,
                'nama_kota' => 'BONE BOLANGO',
            ),
            368 => 
            array (
                'id_kota' => 369,
                'nama_kota' => 'GORONTALO',
            ),
            369 => 
            array (
                'id_kota' => 370,
                'nama_kota' => 'GORONTALO UTARA',
            ),
            370 => 
            array (
                'id_kota' => 371,
                'nama_kota' => 'POHUWATO',
            ),
            371 => 
            array (
                'id_kota' => 372,
                'nama_kota' => 'BITUNG',
            ),
            372 => 
            array (
                'id_kota' => 373,
            'nama_kota' => 'BOLAANG MONGONDOW (BOLMONG)',
            ),
            373 => 
            array (
                'id_kota' => 374,
                'nama_kota' => 'BOLAANG MONGONDOW SELATAN',
            ),
            374 => 
            array (
                'id_kota' => 375,
                'nama_kota' => 'BOLAANG MONGONDOW TIMUR',
            ),
            375 => 
            array (
                'id_kota' => 376,
                'nama_kota' => 'BOLAANG MONGONDOW UTARA',
            ),
            376 => 
            array (
                'id_kota' => 377,
                'nama_kota' => 'KEPULAUAN SANGIHE',
            ),
            377 => 
            array (
                'id_kota' => 378,
            'nama_kota' => 'KEPULAUAN SIAU TAGULANDANG BIARO (SITARO)',
            ),
            378 => 
            array (
                'id_kota' => 379,
                'nama_kota' => 'KEPULAUAN TALAUD',
            ),
            379 => 
            array (
                'id_kota' => 380,
                'nama_kota' => 'KOTAMOBAGU',
            ),
            380 => 
            array (
                'id_kota' => 381,
                'nama_kota' => 'MANADO',
            ),
            381 => 
            array (
                'id_kota' => 382,
                'nama_kota' => 'MINAHASA',
            ),
            382 => 
            array (
                'id_kota' => 383,
                'nama_kota' => 'MINAHASA SELATAN',
            ),
            383 => 
            array (
                'id_kota' => 384,
                'nama_kota' => 'MINAHASA TENGGARA',
            ),
            384 => 
            array (
                'id_kota' => 385,
                'nama_kota' => 'MINAHASA UTARA',
            ),
            385 => 
            array (
                'id_kota' => 386,
                'nama_kota' => 'TOMOHON',
            ),
            386 => 
            array (
                'id_kota' => 387,
                'nama_kota' => 'AMBON',
            ),
            387 => 
            array (
                'id_kota' => 388,
                'nama_kota' => 'BURU',
            ),
            388 => 
            array (
                'id_kota' => 389,
                'nama_kota' => 'BURU SELATAN',
            ),
            389 => 
            array (
                'id_kota' => 390,
                'nama_kota' => 'KEPULAUAN ARU',
            ),
            390 => 
            array (
                'id_kota' => 391,
                'nama_kota' => 'MALUKU BARAT DAYA',
            ),
            391 => 
            array (
                'id_kota' => 392,
                'nama_kota' => 'MALUKU TENGAH',
            ),
            392 => 
            array (
                'id_kota' => 393,
                'nama_kota' => 'MALUKU TENGGARA',
            ),
            393 => 
            array (
                'id_kota' => 394,
                'nama_kota' => 'MALUKU TENGGARA BARAT',
            ),
            394 => 
            array (
                'id_kota' => 395,
                'nama_kota' => 'SERAM BAGIAN BARAT',
            ),
            395 => 
            array (
                'id_kota' => 396,
                'nama_kota' => 'SERAM BAGIAN TIMUR',
            ),
            396 => 
            array (
                'id_kota' => 397,
                'nama_kota' => 'TUAL',
            ),
            397 => 
            array (
                'id_kota' => 398,
                'nama_kota' => 'HALMAHERA BARAT',
            ),
            398 => 
            array (
                'id_kota' => 399,
                'nama_kota' => 'HALMAHERA SELATAN',
            ),
            399 => 
            array (
                'id_kota' => 400,
                'nama_kota' => 'HALMAHERA TENGAH',
            ),
            400 => 
            array (
                'id_kota' => 401,
                'nama_kota' => 'HALMAHERA TIMUR',
            ),
            401 => 
            array (
                'id_kota' => 402,
                'nama_kota' => 'HALMAHERA UTARA',
            ),
            402 => 
            array (
                'id_kota' => 403,
                'nama_kota' => 'KEPULAUAN SULA',
            ),
            403 => 
            array (
                'id_kota' => 404,
                'nama_kota' => 'PULAU MOROTAI',
            ),
            404 => 
            array (
                'id_kota' => 405,
                'nama_kota' => 'TERNATE',
            ),
            405 => 
            array (
                'id_kota' => 406,
                'nama_kota' => 'TIDORE KEPULAUAN',
            ),
            406 => 
            array (
                'id_kota' => 407,
                'nama_kota' => 'BIMA',
            ),
            407 => 
            array (
                'id_kota' => 408,
                'nama_kota' => 'DOMPU',
            ),
            408 => 
            array (
                'id_kota' => 409,
                'nama_kota' => 'LOMBOK BARAT',
            ),
            409 => 
            array (
                'id_kota' => 410,
                'nama_kota' => 'LOMBOK TENGAH',
            ),
            410 => 
            array (
                'id_kota' => 411,
                'nama_kota' => 'LOMBOK TIMUR',
            ),
            411 => 
            array (
                'id_kota' => 412,
                'nama_kota' => 'LOMBOK UTARA',
            ),
            412 => 
            array (
                'id_kota' => 413,
                'nama_kota' => 'MATARAM',
            ),
            413 => 
            array (
                'id_kota' => 414,
                'nama_kota' => 'SUMBAWA',
            ),
            414 => 
            array (
                'id_kota' => 415,
                'nama_kota' => 'SUMBAWA BARAT',
            ),
            415 => 
            array (
                'id_kota' => 416,
                'nama_kota' => 'ALOR',
            ),
            416 => 
            array (
                'id_kota' => 417,
                'nama_kota' => 'BELU',
            ),
            417 => 
            array (
                'id_kota' => 418,
                'nama_kota' => 'ENDE',
            ),
            418 => 
            array (
                'id_kota' => 419,
                'nama_kota' => 'FLORES TIMUR',
            ),
            419 => 
            array (
                'id_kota' => 420,
                'nama_kota' => 'KUPANG',
            ),
            420 => 
            array (
                'id_kota' => 421,
                'nama_kota' => 'LEMBATA',
            ),
            421 => 
            array (
                'id_kota' => 422,
                'nama_kota' => 'MANGGARAI',
            ),
            422 => 
            array (
                'id_kota' => 423,
                'nama_kota' => 'MANGGARAI BARAT',
            ),
            423 => 
            array (
                'id_kota' => 424,
                'nama_kota' => 'MANGGARAI TIMUR',
            ),
            424 => 
            array (
                'id_kota' => 425,
                'nama_kota' => 'NAGEKEO',
            ),
            425 => 
            array (
                'id_kota' => 426,
                'nama_kota' => 'NGADA',
            ),
            426 => 
            array (
                'id_kota' => 427,
                'nama_kota' => 'ROTE NDAO',
            ),
            427 => 
            array (
                'id_kota' => 428,
                'nama_kota' => 'SABU RAIJUA',
            ),
            428 => 
            array (
                'id_kota' => 429,
                'nama_kota' => 'SIKKA',
            ),
            429 => 
            array (
                'id_kota' => 430,
                'nama_kota' => 'SUMBA BARAT',
            ),
            430 => 
            array (
                'id_kota' => 431,
                'nama_kota' => 'SUMBA BARAT DAYA',
            ),
            431 => 
            array (
                'id_kota' => 432,
                'nama_kota' => 'SUMBA TENGAH',
            ),
            432 => 
            array (
                'id_kota' => 433,
                'nama_kota' => 'SUMBA TIMUR',
            ),
            433 => 
            array (
                'id_kota' => 434,
                'nama_kota' => 'TIMOR TENGAH SELATAN',
            ),
            434 => 
            array (
                'id_kota' => 435,
                'nama_kota' => 'TIMOR TENGAH UTARA',
            ),
            435 => 
            array (
                'id_kota' => 436,
                'nama_kota' => 'FAKFAK',
            ),
            436 => 
            array (
                'id_kota' => 437,
                'nama_kota' => 'KAIMANA',
            ),
            437 => 
            array (
                'id_kota' => 438,
                'nama_kota' => 'MANOKWARI',
            ),
            438 => 
            array (
                'id_kota' => 439,
                'nama_kota' => 'MANOKWARI SELATAN',
            ),
            439 => 
            array (
                'id_kota' => 440,
                'nama_kota' => 'MAYBRAT',
            ),
            440 => 
            array (
                'id_kota' => 441,
                'nama_kota' => 'PEGUNUNGAN ARFAK',
            ),
            441 => 
            array (
                'id_kota' => 442,
                'nama_kota' => 'RAJA AMPAT',
            ),
            442 => 
            array (
                'id_kota' => 443,
                'nama_kota' => 'SORONG',
            ),
            443 => 
            array (
                'id_kota' => 444,
                'nama_kota' => 'SORONG SELATAN',
            ),
            444 => 
            array (
                'id_kota' => 445,
                'nama_kota' => 'TAMBRAUW',
            ),
            445 => 
            array (
                'id_kota' => 446,
                'nama_kota' => 'TELUK BINTUNI',
            ),
            446 => 
            array (
                'id_kota' => 447,
                'nama_kota' => 'TELUK WONDAMA',
            ),
            447 => 
            array (
                'id_kota' => 448,
                'nama_kota' => 'ASMAT',
            ),
            448 => 
            array (
                'id_kota' => 449,
                'nama_kota' => 'BIAK NUMFOR',
            ),
            449 => 
            array (
                'id_kota' => 450,
                'nama_kota' => 'BOVEN DIGOEL',
            ),
            450 => 
            array (
                'id_kota' => 451,
            'nama_kota' => 'DEIYAI (DELIYAI)',
            ),
            451 => 
            array (
                'id_kota' => 452,
                'nama_kota' => 'DOGIYAI',
            ),
            452 => 
            array (
                'id_kota' => 453,
                'nama_kota' => 'INTAN JAYA',
            ),
            453 => 
            array (
                'id_kota' => 454,
                'nama_kota' => 'JAYAPURA',
            ),
            454 => 
            array (
                'id_kota' => 455,
                'nama_kota' => 'JAYAWIJAYA',
            ),
            455 => 
            array (
                'id_kota' => 456,
                'nama_kota' => 'KEEROM',
            ),
            456 => 
            array (
                'id_kota' => 457,
            'nama_kota' => 'KEPULAUAN YAPEN (YAPEN WAROPEN)',
            ),
            457 => 
            array (
                'id_kota' => 458,
                'nama_kota' => 'LANNY JAYA',
            ),
            458 => 
            array (
                'id_kota' => 459,
                'nama_kota' => 'MAMBERAMO RAYA',
            ),
            459 => 
            array (
                'id_kota' => 460,
                'nama_kota' => 'MAMBERAMO TENGAH',
            ),
            460 => 
            array (
                'id_kota' => 461,
                'nama_kota' => 'MAPPI',
            ),
            461 => 
            array (
                'id_kota' => 462,
                'nama_kota' => 'MERAUKE',
            ),
            462 => 
            array (
                'id_kota' => 463,
                'nama_kota' => 'MIMIKA',
            ),
            463 => 
            array (
                'id_kota' => 464,
                'nama_kota' => 'NABIRE',
            ),
            464 => 
            array (
                'id_kota' => 465,
                'nama_kota' => 'NDUGA',
            ),
            465 => 
            array (
                'id_kota' => 466,
                'nama_kota' => 'PANIAI',
            ),
            466 => 
            array (
                'id_kota' => 467,
                'nama_kota' => 'PEGUNUNGAN BINTANG',
            ),
            467 => 
            array (
                'id_kota' => 468,
                'nama_kota' => 'PUNCAK',
            ),
            468 => 
            array (
                'id_kota' => 469,
                'nama_kota' => 'PUNCAK JAYA',
            ),
            469 => 
            array (
                'id_kota' => 470,
                'nama_kota' => 'SARMI',
            ),
            470 => 
            array (
                'id_kota' => 471,
                'nama_kota' => 'SUPIORI',
            ),
            471 => 
            array (
                'id_kota' => 472,
                'nama_kota' => 'TOLIKARA',
            ),
            472 => 
            array (
                'id_kota' => 473,
                'nama_kota' => 'WAROPEN',
            ),
            473 => 
            array (
                'id_kota' => 474,
                'nama_kota' => 'YAHUKIMO',
            ),
            474 => 
            array (
                'id_kota' => 475,
                'nama_kota' => 'YALIMO',
            ),
            475 => 
            array (
                'id_kota' => 476,
                'nama_kota' => 'JAKARTA',
            ),
            476 => 
            array (
                'id_kota' => 477,
                'nama_kota' => 'SOLO',
            ),
            477 => 
            array (
                'id_kota' => 478,
                'nama_kota' => 'SERIRIT',
            ),
            478 => 
            array (
                'id_kota' => 479,
                'nama_kota' => 'PADANGSIDEMPUAN',
            ),
            479 => 
            array (
                'id_kota' => 480,
                'nama_kota' => 'WATU',
            ),
            480 => 
            array (
                'id_kota' => 481,
                'nama_kota' => 'KAB. SEMARANG',
            ),
            481 => 
            array (
                'id_kota' => 482,
                'nama_kota' => 'LARANTUKA',
            ),
            482 => 
            array (
                'id_kota' => 483,
                'nama_kota' => 'DAMPANG',
            ),
            483 => 
            array (
                'id_kota' => 484,
                'nama_kota' => 'CIBINONG',
            ),
            484 => 
            array (
                'id_kota' => 485,
                'nama_kota' => 'SUNGAILIAT',
            ),
            485 => 
            array (
                'id_kota' => 486,
                'nama_kota' => 'LAMPUNG',
            ),
            486 => 
            array (
                'id_kota' => 487,
                'nama_kota' => 'PADANG BINTUNGAN',
            ),
            487 => 
            array (
                'id_kota' => 488,
                'nama_kota' => 'PURWOKERTO',
            ),
        ));
        
        
    }
}