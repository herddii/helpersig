<?php

use Illuminate\Database\Seeder;

class TblParameterTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_parameter')->delete();
        
        \DB::table('tbl_parameter')->insert(array (
            0 => 
            array (
                'id_parameter' => 1,
                'nama_parameter' => 'Arsen, Arsen Anorganik, Arsen Total, Merkuri',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id_parameter' => 2,
                'nama_parameter' => 'Besi',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id_parameter' => 3,
                'nama_parameter' => 'Aluminium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id_parameter' => 4,
                'nama_parameter' => 'Merkuri',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id_parameter' => 5,
                'nama_parameter' => 'Arsen',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id_parameter' => 6,
                'nama_parameter' => 'Arsen Anorganik',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id_parameter' => 7,
                'nama_parameter' => 'Timbal',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id_parameter' => 8,
                'nama_parameter' => 'Triamsinolon Asetonida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id_parameter' => 9,
                'nama_parameter' => 'Methanyl Yellow',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id_parameter' => 10,
                'nama_parameter' => 'Rhodamin B',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id_parameter' => 11,
                'nama_parameter' => 'Asam retinoat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id_parameter' => 12,
                'nama_parameter' => 'Vitamin B1',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id_parameter' => 13,
                'nama_parameter' => 'Histamin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id_parameter' => 14,
                'nama_parameter' => 'L-Triptophane',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id_parameter' => 15,
                'nama_parameter' => 'K-Sorbat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id_parameter' => 16,
                'nama_parameter' => 'Chloramphenicol',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id_parameter' => 17,
                'nama_parameter' => 'Betametason',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id_parameter' => 18,
                'nama_parameter' => 'Hidrokinon',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id_parameter' => 19,
            'nama_parameter' => 'Kafein (anhidrat)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id_parameter' => 20,
                'nama_parameter' => 'Glycine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id_parameter' => 21,
                'nama_parameter' => 'Okratoksin A',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id_parameter' => 22,
                'nama_parameter' => 'Vitamin C',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id_parameter' => 23,
            'nama_parameter' => 'Alkohol (miras)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id_parameter' => 24,
                'nama_parameter' => 'Vitamin B12',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id_parameter' => 25,
                'nama_parameter' => 'Kolesterol',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id_parameter' => 26,
                'nama_parameter' => 'Kadar lemak, Asam lemak bebas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id_parameter' => 27,
                'nama_parameter' => 'Kadar protein',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id_parameter' => 28,
            'nama_parameter' => 'Karbon dioksida (CO2) bebas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id_parameter' => 29,
                'nama_parameter' => 'Daya hantar listrik pada 25 ?C',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id_parameter' => 30,
                'nama_parameter' => 'Minyak pelikan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id_parameter' => 31,
                'nama_parameter' => 'Aldrin dan Dieldrin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id_parameter' => 32,
                'nama_parameter' => 'Heptachlorepoxide',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id_parameter' => 33,
                'nama_parameter' => 'pH',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id_parameter' => 34,
                'nama_parameter' => 'Kekeruhan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id_parameter' => 35,
                'nama_parameter' => 'Zat yang terlarut',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id_parameter' => 36,
            'nama_parameter' => 'Klorida (Cl-)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id_parameter' => 37,
                'nama_parameter' => 'Kadar air',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id_parameter' => 38,
            'nama_parameter' => 'Amonium (NH4)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id_parameter' => 39,
                'nama_parameter' => 'Total Organik Karbon ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id_parameter' => 40,
            'nama_parameter' => 'Fluorida (F)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id_parameter' => 41,
            'nama_parameter' => 'Sianida (CN)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id_parameter' => 42,
                'nama_parameter' => 'Kafein',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id_parameter' => 43,
                'nama_parameter' => 'Saturated Fat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id_parameter' => 44,
            'nama_parameter' => 'Kehalusan, lolos ayakan 212 um No. 70 (b/b)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id_parameter' => 45,
                'nama_parameter' => 'Bilangan Peroksida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id_parameter' => 46,
                'nama_parameter' => 'Azo dyes :',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id_parameter' => 47,
                'nama_parameter' => 'Formaldehyde',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id_parameter' => 48,
            'nama_parameter' => 'Total Volatile Base-Nitrogen (TVB-N) ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id_parameter' => 49,
                'nama_parameter' => 'Asam Lemak',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id_parameter' => 50,
                'nama_parameter' => 'Siklamat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id_parameter' => 51,
                'nama_parameter' => 'Fruktosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id_parameter' => 52,
                'nama_parameter' => 'Xylosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id_parameter' => 53,
                'nama_parameter' => 'Vitamin B9',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id_parameter' => 54,
                'nama_parameter' => 'EDTA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id_parameter' => 55,
                'nama_parameter' => 'Formalin Kualitatif',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id_parameter' => 56,
                'nama_parameter' => 'Aflatoksin G1',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id_parameter' => 57,
                'nama_parameter' => 'Warna Lovibond',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id_parameter' => 58,
            'nama_parameter' => 'Surfaktan Anionik (Deterjen)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id_parameter' => 59,
                'nama_parameter' => 'Amonium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id_parameter' => 60,
                'nama_parameter' => 'Flourida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id_parameter' => 61,
                'nama_parameter' => 'Nitrat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id_parameter' => 62,
                'nama_parameter' => 'Nitrit',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id_parameter' => 63,
                'nama_parameter' => 'Klor Bebas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id_parameter' => 64,
                'nama_parameter' => 'Sianida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id_parameter' => 65,
                'nama_parameter' => 'Sulfat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id_parameter' => 66,
                'nama_parameter' => 'Warna',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id_parameter' => 67,
                'nama_parameter' => 'ALT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id_parameter' => 68,
                'nama_parameter' => 'Kapang',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id_parameter' => 69,
                'nama_parameter' => 'Bacillus cereus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id_parameter' => 70,
                'nama_parameter' => 'Listeria monocytogenes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id_parameter' => 71,
                'nama_parameter' => 'Clostridium perfringens',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id_parameter' => 72,
                'nama_parameter' => 'ALT Thermofilik Pembentuk Spora',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id_parameter' => 73,
                'nama_parameter' => 'Legionella pneumophilla',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id_parameter' => 74,
                'nama_parameter' => 'Enterobactericeae',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id_parameter' => 75,
                'nama_parameter' => 'Salmonella sp',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id_parameter' => 76,
                'nama_parameter' => 'Escherichia coli',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id_parameter' => 77,
                'nama_parameter' => 'Vibrio cholerae',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id_parameter' => 78,
                'nama_parameter' => 'Pseudomonas aeruginosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id_parameter' => 79,
                'nama_parameter' => 'Enterococci',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id_parameter' => 80,
                'nama_parameter' => 'Khamir',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id_parameter' => 81,
                'nama_parameter' => 'Bacillus sp',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id_parameter' => 82,
                'nama_parameter' => 'Staphyloccoucus aureus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id_parameter' => 83,
                'nama_parameter' => 'Coliform',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id_parameter' => 84,
                'nama_parameter' => 'Anaerob pereduksi sulfit pembentuk spora',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id_parameter' => 85,
                'nama_parameter' => 'Candida albicans',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id_parameter' => 86,
                'nama_parameter' => 'Shigella sonnei',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id_parameter' => 87,
                'nama_parameter' => 'Vibrio parahaemolyticus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id_parameter' => 88,
                'nama_parameter' => 'Bifidobacterium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id_parameter' => 89,
                'nama_parameter' => 'Lactobacillus acidophilus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id_parameter' => 90,
                'nama_parameter' => 'Lactobacillus casei',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id_parameter' => 91,
                'nama_parameter' => 'Enterobacteriaceae plate count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id_parameter' => 92,
                'nama_parameter' => 'S.aureus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id_parameter' => 93,
                'nama_parameter' => 'Enterobacteriaceae kualitatif',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id_parameter' => 94,
                'nama_parameter' => 'B.cereus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id_parameter' => 95,
                'nama_parameter' => 'P.aeruginosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id_parameter' => 96,
                'nama_parameter' => 'UDH',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id_parameter' => 97,
                'nama_parameter' => 'Efektivitas Pengawet',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id_parameter' => 98,
                'nama_parameter' => 'E.coli',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id_parameter' => 99,
                'nama_parameter' => 'E.coli Plate Count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id_parameter' => 100,
                'nama_parameter' => 'Enterobactericeae plate count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id_parameter' => 101,
                'nama_parameter' => 'Enterobacteariceae plate count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id_parameter' => 102,
                'nama_parameter' => 'Enterobacteariceae kualitatif',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id_parameter' => 103,
                'nama_parameter' => 'Enterobacter sakazakii',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id_parameter' => 104,
            'nama_parameter' => 'Formalin (kualitatif)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id_parameter' => 105,
            'nama_parameter' => 'Vitamin B2 (Riboflavin)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id_parameter' => 106,
                'nama_parameter' => 'Indigotin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id_parameter' => 107,
                'nama_parameter' => 'Tartrazin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id_parameter' => 108,
                'nama_parameter' => 'Betametason 17 Valerate',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id_parameter' => 109,
                'nama_parameter' => 'Deksametason',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id_parameter' => 110,
                'nama_parameter' => 'Hidrokortison asetat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id_parameter' => 111,
            'nama_parameter' => 'Quinoline Yellow (E104)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id_parameter' => 112,
                'nama_parameter' => 'Glukosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id_parameter' => 113,
                'nama_parameter' => 'Sakarosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id_parameter' => 114,
                'nama_parameter' => 'Maltosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id_parameter' => 115,
                'nama_parameter' => 'Laktosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id_parameter' => 116,
                'nama_parameter' => 'L-Alanine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id_parameter' => 117,
                'nama_parameter' => 'L-Argrinine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id_parameter' => 118,
                'nama_parameter' => 'L-Aspartic acid',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id_parameter' => 119,
                'nama_parameter' => 'L-Cystine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id_parameter' => 120,
                'nama_parameter' => 'L-Glutamic Acid',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id_parameter' => 121,
                'nama_parameter' => 'L-Histidine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id_parameter' => 122,
                'nama_parameter' => 'L-Isoleusine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id_parameter' => 123,
                'nama_parameter' => 'L-L Lysine HCl',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id_parameter' => 124,
                'nama_parameter' => 'L-Leucine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id_parameter' => 125,
                'nama_parameter' => 'L-Methionine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id_parameter' => 126,
                'nama_parameter' => 'L-Phenylalanine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id_parameter' => 127,
                'nama_parameter' => 'L-Proline',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id_parameter' => 128,
                'nama_parameter' => 'L-Serine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id_parameter' => 129,
                'nama_parameter' => 'L-Threonine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id_parameter' => 130,
                'nama_parameter' => 'L-Tyrosine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id_parameter' => 131,
                'nama_parameter' => 'L-Valine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id_parameter' => 132,
                'nama_parameter' => 'L-Triptophan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id_parameter' => 133,
                'nama_parameter' => 'Total Glukosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id_parameter' => 134,
                'nama_parameter' => 'Aflatoksin total, B1, B2, G1 dan G2',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id_parameter' => 135,
                'nama_parameter' => 'Vitamin E Asetat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id_parameter' => 136,
            'nama_parameter' => 'Butylated Hydroxytoluene (BHT)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id_parameter' => 137,
            'nama_parameter' => 'Butylated Hydroxyanisole (BHA)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id_parameter' => 138,
            'nama_parameter' => 'Tertiary Butylhydroquinone (TBHQ)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id_parameter' => 139,
                'nama_parameter' => 'Sudan I',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id_parameter' => 140,
                'nama_parameter' => 'Sudan II',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id_parameter' => 141,
                'nama_parameter' => 'Sudan III',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id_parameter' => 142,
                'nama_parameter' => 'Sudan IV',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id_parameter' => 143,
                'nama_parameter' => 'Amitrol',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id_parameter' => 144,
                'nama_parameter' => 'Residu Pestisida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id_parameter' => 145,
                'nama_parameter' => 'Diquat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id_parameter' => 146,
                'nama_parameter' => 'Identifikasi Gelatin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id_parameter' => 147,
                'nama_parameter' => 'Glufosinate Ammonium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id_parameter' => 148,
                'nama_parameter' => 'Azo Dyes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id_parameter' => 149,
                'nama_parameter' => 'Tetrasiklin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id_parameter' => 150,
                'nama_parameter' => '3MCPD',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id_parameter' => 151,
                'nama_parameter' => 'Pthalat',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id_parameter' => 152,
                'nama_parameter' => 'Dioksin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id_parameter' => 153,
                'nama_parameter' => 'Klorampenikol',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id_parameter' => 154,
                'nama_parameter' => 'Melamine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id_parameter' => 155,
                'nama_parameter' => 'Dimethipin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id_parameter' => 156,
                'nama_parameter' => 'TPM dyes',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id_parameter' => 157,
                'nama_parameter' => 'Residu Solvent',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id_parameter' => 158,
                'nama_parameter' => 'Kadar Fruktosa',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id_parameter' => 159,
            'nama_parameter' => 'Kadar Dimethylaminoethanol Hydrogentartrate (DMAE)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id_parameter' => 160,
                'nama_parameter' => 'Kadar Folic Acid',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id_parameter' => 161,
                'nama_parameter' => 'Kadar Ascorbic Acid',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id_parameter' => 162,
                'nama_parameter' => 'Kadar Cholecalciferol ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id_parameter' => 163,
                'nama_parameter' => 'Kadar L-Tryptophan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id_parameter' => 164,
                'nama_parameter' => 'Kadar L-Lysine Acetate, L-Histidine, L-Threonine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id_parameter' => 165,
                'nama_parameter' => 'Kadar Biotin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id_parameter' => 166,
                'nama_parameter' => 'Okratoksin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id_parameter' => 167,
            'nama_parameter' => 'Deoxinivalenol (DON)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id_parameter' => 168,
            'nama_parameter' => 'Oksigen Terlarut (DO)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id_parameter' => 169,
                'nama_parameter' => 'Ca',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id_parameter' => 170,
                'nama_parameter' => 'Mg',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id_parameter' => 171,
                'nama_parameter' => 'Na',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id_parameter' => 172,
                'nama_parameter' => 'Zn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id_parameter' => 173,
                'nama_parameter' => 'Al',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id_parameter' => 174,
                'nama_parameter' => 'Fe',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id_parameter' => 175,
                'nama_parameter' => 'As total',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id_parameter' => 176,
                'nama_parameter' => 'Pb',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id_parameter' => 177,
                'nama_parameter' => 'Cd',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id_parameter' => 178,
                'nama_parameter' => 'Hg',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id_parameter' => 179,
                'nama_parameter' => 'As Anorganik',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id_parameter' => 180,
                'nama_parameter' => 'TiO2',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id_parameter' => 181,
                'nama_parameter' => 'Dimethipine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id_parameter' => 182,
                'nama_parameter' => '3-MCPD',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id_parameter' => 183,
                'nama_parameter' => 'As',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id_parameter' => 184,
                'nama_parameter' => 'Sn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id_parameter' => 185,
                'nama_parameter' => 'Melamin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id_parameter' => 186,
                'nama_parameter' => 'Cadmium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id_parameter' => 187,
                'nama_parameter' => 'Timah',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id_parameter' => 188,
                'nama_parameter' => 'Kadar Abu',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id_parameter' => 189,
                'nama_parameter' => 'Padatan Total',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id_parameter' => 190,
                'nama_parameter' => 'Gula Total',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id_parameter' => 191,
                'nama_parameter' => 'Kadar Lemak',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id_parameter' => 192,
                'nama_parameter' => 'NaCl',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id_parameter' => 193,
                'nama_parameter' => 'Klorida',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id_parameter' => 194,
                'nama_parameter' => 'Alkalinitas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id_parameter' => 195,
                'nama_parameter' => 'Bilangan Iod',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id_parameter' => 196,
                'nama_parameter' => 'Bilangan Penyabunan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id_parameter' => 197,
                'nama_parameter' => 'Kesadahan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id_parameter' => 198,
                'nama_parameter' => 'Sulfit',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id_parameter' => 199,
                'nama_parameter' => 'Kadar Ekstrak Panax Ginseng Terstandar ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id_parameter' => 200,
                'nama_parameter' => 'Kadar L-Tyrosine',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id_parameter' => 201,
            'nama_parameter' => 'Angka Lempeng Total (ALT)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id_parameter' => 202,
                'nama_parameter' => 'Enterobacteariceae',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id_parameter' => 203,
                'nama_parameter' => 'Escherichia coli Plate Count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id_parameter' => 204,
                'nama_parameter' => 'Enterobacteriaceae',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id_parameter' => 205,
                'nama_parameter' => 'Staphylococcus aureus',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id_parameter' => 206,
                'nama_parameter' => 'Clostridium perfringens, Shigella sp',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id_parameter' => 207,
                'nama_parameter' => 'Endotoksin ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id_parameter' => 208,
                'nama_parameter' => 'Endotoksin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id_parameter' => 209,
                'nama_parameter' => 'Kapang, Khamir, Candida albicans, ALT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id_parameter' => 210,
                'nama_parameter' => 'Coliform, Escherichia coli, Escherichia coli plate count',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id_parameter' => 211,
                'nama_parameter' => 'Fe, Zn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id_parameter' => 212,
                'nama_parameter' => 'Pb, Cd, Hg, As',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id_parameter' => 213,
                'nama_parameter' => 'Mn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id_parameter' => 214,
                'nama_parameter' => 'Mo, Se',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id_parameter' => 215,
                'nama_parameter' => 'Cd, Hg, As',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id_parameter' => 216,
                'nama_parameter' => 'Sodium Cu Chlorophyllin, Fe, Zn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id_parameter' => 217,
                'nama_parameter' => 'As III, As V',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id_parameter' => 218,
                'nama_parameter' => 'Arsenobetaine, Dimetylarsenic acid',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id_parameter' => 219,
                'nama_parameter' => 'Ca, Na',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id_parameter' => 220,
            'nama_parameter' => 'Cu, Ni, Pb, Cd (Logam Terkestraksi)',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id_parameter' => 221,
                'nama_parameter' => 'Sulfur',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id_parameter' => 222,
                'nama_parameter' => 'Posfor',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id_parameter' => 223,
                'nama_parameter' => 'Pb, Hg',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id_parameter' => 224,
                'nama_parameter' => 'Cd, As',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id_parameter' => 225,
                'nama_parameter' => 'Mo',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id_parameter' => 226,
                'nama_parameter' => 'Cu, Fe, Zn, Sn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id_parameter' => 227,
                'nama_parameter' => 'Ca, Sn',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id_parameter' => 228,
                'nama_parameter' => 'Cu',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id_parameter' => 229,
                'nama_parameter' => 'Mn, Cr',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id_parameter' => 230,
                'nama_parameter' => 'K, Fe, Ca, Cu, Mg',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id_parameter' => 231,
                'nama_parameter' => 'SiO2',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id_parameter' => 232,
                'nama_parameter' => 'Al, Ca, Fe, Mg',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id_parameter' => 233,
                'nama_parameter' => 'Kadar Air KF',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id_parameter' => 234,
                'nama_parameter' => 'Gula Reduksi',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id_parameter' => 235,
                'nama_parameter' => 'Asam Lemak Bebas',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id_parameter' => 236,
                'nama_parameter' => 'Bilangan Asam',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id_parameter' => 237,
                'nama_parameter' => 'Serat Pangan Total',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id_parameter' => 238,
                'nama_parameter' => 'Serat Kasar',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id_parameter' => 239,
                'nama_parameter' => 'Zat Organik',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id_parameter' => 240,
                'nama_parameter' => 'Formalin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id_parameter' => 241,
                'nama_parameter' => 'Curcumin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id_parameter' => 242,
                'nama_parameter' => 'Aspartame',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id_parameter' => 243,
                'nama_parameter' => 'Detergen',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id_parameter' => 244,
                'nama_parameter' => 'Triptophan',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id_parameter' => 245,
                'nama_parameter' => 'Antioksidan AOAC',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id_parameter' => 246,
                'nama_parameter' => 'Sudan Red',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id_parameter' => 247,
                'nama_parameter' => 'Pewarna',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id_parameter' => 248,
                'nama_parameter' => 'Vitamin B',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id_parameter' => 249,
                'nama_parameter' => 'Pengawet Kosmetik',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id_parameter' => 250,
                'nama_parameter' => 'Asam Lemak ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id_parameter' => 251,
                'nama_parameter' => 'Kolesterol ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id_parameter' => 252,
                'nama_parameter' => '3 MCPD ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id_parameter' => 253,
                'nama_parameter' => 'Etanol ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id_parameter' => 254,
                'nama_parameter' => 'Metanol ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id_parameter' => 255,
                'nama_parameter' => 'Residu Solvent ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id_parameter' => 256,
                'nama_parameter' => 'Multi Residu Pestisida LCMSMS ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id_parameter' => 257,
                'nama_parameter' => 'Multi Residu Pestisida GCMSMS ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id_parameter' => 258,
                'nama_parameter' => 'Diquat ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id_parameter' => 259,
                'nama_parameter' => 'Chlormequat ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id_parameter' => 260,
                'nama_parameter' => 'Maleic Hydrazide',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id_parameter' => 261,
                'nama_parameter' => 'Ethephone ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id_parameter' => 262,
                'nama_parameter' => 'Ion Bromida ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id_parameter' => 263,
                'nama_parameter' => 'Tetracycline ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id_parameter' => 264,
                'nama_parameter' => 'Oxytetracycline ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id_parameter' => 265,
                'nama_parameter' => 'Chlortetracycline',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id_parameter' => 266,
                'nama_parameter' => 'Vitamin B12 ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id_parameter' => 267,
                'nama_parameter' => 'Chloramphenicol ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id_parameter' => 268,
                'nama_parameter' => 'Mikotoksin ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id_parameter' => 269,
                'nama_parameter' => 'Dioksin ',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}