<?php

use Illuminate\Database\Seeder;

class TblStatusPegawaiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_status_pegawai')->delete();
        
        \DB::table('tbl_status_pegawai')->insert(array (
            0 => 
            array (
                'id_status_pegawai' => 1,
                'status_pegawai' => 'TETAP',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id_status_pegawai' => 2,
                'status_pegawai' => 'KONTRAK',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id_status_pegawai' => 3,
                'status_pegawai' => 'HONORER',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}