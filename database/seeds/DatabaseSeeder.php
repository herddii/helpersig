<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(HrisEmployeeTableSeeder::class);
        $this->call(TblPendidikanKaryawanTableSeeder::class);
        $this->call(TblKotaTableSeeder::class);
        $this->call(TblPengalamanKerjaTableSeeder::class);
        $this->call(TblPengujiTableSeeder::class);
        $this->call(TblStatusPegawaiTableSeeder::class);
        $this->call(TblLevelTableSeeder::class);
        $this->call(TblJabatanTableSeeder::class);
        $this->call(TblKompetensiTableSeeder::class);
        $this->call(TblParameterTableSeeder::class);
        $this->call(PrinsipalTableSeeder::class);
        $this->call(MstrCustomersCustomerTableSeeder::class);
        $this->call(MstrCustomersTaxaddressTableSeeder::class);
        $this->call(MstrCustomersContactpersonTableSeeder::class);
        $this->call(MstrCustomersAddressTableSeeder::class);
    }
}
