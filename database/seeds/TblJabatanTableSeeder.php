<?php

use Illuminate\Database\Seeder;

class TblJabatanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_jabatan')->delete();
        
        \DB::table('tbl_jabatan')->insert(array (
            0 => 
            array (
                'id_jabatan' => 1,
                'nama_jabatan' => 'Direktur Operasional',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id_jabatan' => 2,
                'nama_jabatan' => 'Manager Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id_jabatan' => 3,
                'nama_jabatan' => 'Direktur Teknik',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id_jabatan' => 4,
                'nama_jabatan' => 'Asisten Manager Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id_jabatan' => 5,
                'nama_jabatan' => 'Manager Laboratorium R & D',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id_jabatan' => 6,
                'nama_jabatan' => 'Manager Mutu',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id_jabatan' => 7,
                'nama_jabatan' => 'Manager Marketing',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id_jabatan' => 8,
                'nama_jabatan' => 'Manager Administrasi',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id_jabatan' => 9,
                'nama_jabatan' => 'Staff Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id_jabatan' => 10,
                'nama_jabatan' => 'Marketing Executive',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id_jabatan' => 11,
                'nama_jabatan' => 'Supervisor Marketing',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id_jabatan' => 12,
                'nama_jabatan' => 'Asisten Manager Marketing',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id_jabatan' => 13,
                'nama_jabatan' => 'Asisten Manager Sales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id_jabatan' => 14,
                'nama_jabatan' => 'Manager Sales',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id_jabatan' => 15,
                'nama_jabatan' => 'Analis Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id_jabatan' => 16,
                'nama_jabatan' => 'Supervisor Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id_jabatan' => 17,
                'nama_jabatan' => 'Teknisi Kantor',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id_jabatan' => 18,
                'nama_jabatan' => 'Teknisi Laboratorium',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id_jabatan' => 19,
                'nama_jabatan' => 'Staff Umum',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id_jabatan' => 20,
                'nama_jabatan' => 'Customer Service',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id_jabatan' => 21,
                'nama_jabatan' => 'Supervisor Administrasi',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id_jabatan' => 22,
                'nama_jabatan' => 'Analis R & D',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id_jabatan' => 23,
                'nama_jabatan' => 'Supervisor R & D',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id_jabatan' => 24,
                'nama_jabatan' => 'Asisten Manager R & D',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id_jabatan' => 25,
                'nama_jabatan' => 'Staff IT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id_jabatan' => 26,
                'nama_jabatan' => 'Supervisor IT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id_jabatan' => 27,
                'nama_jabatan' => 'Petugas Sampel',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id_jabatan' => 28,
                'nama_jabatan' => 'Staff Legal & GA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id_jabatan' => 29,
                'nama_jabatan' => 'Supervisor HRD & GA',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}