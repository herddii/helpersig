            <!-- Sidebar scroll-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">--- HELPER</li>
                        <li>
                            <a href="{{URL::to('standart')}}">
                                <i class="fas fa-copy"></i>
                                <span class="hide-menu">Standard
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::to('hasiluji')}}">
                                <i class="fas fa-print"></i>
                                <span class="hide-menu">Hasil Uji
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                <i class="fas fa-clipboard-list"></i>
                                <span class="hide-menu">Routine Recap</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{URL::to('testing')}}">Sales </a>
                                </li>
                                <li>
                                    <a href="index2.html">Sample</a>
                                </li>
                                <li>
                                    <a href="index3.html">Contract</a>
                                </li>
                                <li>
                                    <a href="index4.html">Uji</a>
                                </li>
                                <li>
                                    <a href="index4.html">Analis</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{URL::to('chat')}}">
                                <i class="fas fa-print"></i>
                                <span class="hide-menu">Chat
                                </span>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0)" class="active">
                                <i class="fas fa-clipboard-list"></i>
                                <span class="hide-menu">Rekap Farmasi
                                </span>
                            </a>
                        </li> -->
                        <li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->