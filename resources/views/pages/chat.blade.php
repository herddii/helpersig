@extends('layouts.dashboard')
@section('content')
<style>
    .actives {
        background: rgba(66, 245, 93, 0.4)
    }
</style>
<div class="container-fluid" style="padding: 0 10px !important;">
    <div class="row">
        <div class="col-12">
            <div class="card" style="
            margin-top: 20px;
            height: 100%;
            margin-bottom: 0px;
            padding: 0px;">
                <!-- .chat-row -->
                <div class="chat-main-box">
                    <!-- .chat-left-panel -->
                    <input id="authuser" value="{{Auth::user()->id}}" type="hidden"/>
                    <div class="chat-left-aside">
                        <div class="open-panel"><i class="ti-angle-right"></i></div>
                        <div class="chat-left-inner">
                            <div class="form-material">
                                <input class="form-control p-2" type="text" placeholder="Search Contact" id="searchContact">
                            </div>
                            <ul class="chatonline style-none" id="onlineList" style="white-space: nowrap;">
                                <li class="p-20"></li>
                            </ul>
                        </div>
                    </div>
                    <!-- .chat-left-panel -->
                    <!-- .chat-right-panel -->
                    <div class="chat-right-aside startchat" style="display: none">
                        <div class="chat-main-header">
                            <div class="p-3 b-b">
                                <h4 class="box-title">Chat Message</h4>
                            </div>
                        </div>
                        <div class="chat-rbox">
                            <ul class="chat-list p-3" id="isichat" style="height: 300px !important;">
                            </ul>
                        </div>
                        <div class="card-body border-top">
                            <div class="row">
                                <div class="col-8">
                                    <textarea placeholder="Type your message here" class="form-control border-0" id="isitext"></textarea>
                                </div>
                                <div class="col-4 text-right">
                                    <button type="button" class="btn btn-info btn-circle btn-lg"><i class="fas fa-paper-plane" id="addchat"></i> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .chat-right-panel -->
                    <div class="chat-right-aside gifchat">
                    <div style="padding: 30px; display: flex; justify-content: center; align-items: center; flex-direction: column">
                    <img src="{{asset('assets/img/source.gif')}}" style="width: 100%;"/>
                    <span>Please select user to interact with .. </span>
                    </div>
                    </div>
                </div>
                <!-- /.chat-row -->
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="{{asset('socket.io-client-master/dist/socket.io.js')}}"></script>
<script>
        var to = [];
        var socket = io.connect('http://10.10.10.87:3000');
    $(()=>{
        getonlinelist('nosearch'); 

    })
    
    $('#searchContact').on('keyup',function(){
        $('#onlineList').empty();
        if($(this).val().length > 3){
            getonlinelist($(this).val());
        } else {
            getonlinelist('nosearch');
        }
    })

    async function getonlinelist(set){
        var v = '';
        var search = set === 'nosearch' ? null : set.toUpperCase();
        await $.get("{{URL::to('chat')}}",{data: search},(a)=>{
           
            a.forEach((c,cc) => {
                var image = c.gender === 'Male' ? 'man-user' : 'female-user';
                v += `
                <li id="ls_${cc}" class="">
                    <a href="javascript:void(0)" onClick="active(${cc},${a.length},${c.employee_id})">
                        <img src="{{asset('assets/img/${image}.png')}}" alt="user-img" class="img-circle"> 
                        <span>${c.employee_name}<small class="text-success">online</small></span>
                    </a>
                </li>`;
            })

            $('#onlineList').append(v);
        })
    }
    function addzero(i){
        return i > 10 ? i : `0${i}`;
    }
    function active(val,length,id){
        to = [];
        $(".startchat").css('display','block');
        $(".gifchat").css('display','none');
        $('#isichat').empty();
        for(let i=0; i < length; i++){
            $('#ls_'+i).removeClass('actives');
        }
        $('#ls_'+val).addClass('actives');
        
        to.push(id);
        
        socket.emit('viewmessage',{data: to[0]});
        socket.on('view', function(data){
            $('#isichat').empty();
            var g = '';
            console.log(data);
            data.forEach((bb,aa) => {
                var dated = new Date(bb.created_at);
                var dated_year = dated.getFullYear();
                var dated_month = dated.toLocaleString('default', { month: 'short' });
                var dated_dated = dated.getDate();
                if(parseInt($('#authuser').val()) === bb.untuk_id){
                g+=`
                <li class="reverse">
                    <div class="chat-content">
                        <div class="box bg-light-inverse" style="background: rgba(66, 245, 93, 0.4)">${bb.chat}</div>
                        <div class="chat-time" style="margin: 5px 0 15px 37px;">${addzero(dated_dated)} - ${dated_month} - ${dated_year}</div>
                    </div>
                    <div class="chat-img">
                        <img src="{{asset('assets/img/man-user.png')}}" alt="user" />
                    </div>
                </li>
                `
                } else {    
                    console.log({auth: $('#authuser').val(), untuk: bb.untuk_id })
                g += `
                <li>
                    <div class="chat-img">
                        <img src="{{asset('assets/img/man-user.png')}}" alt="user" />
                    </div>
                    <div class="chat-content">
                        <input id="value_to_${aa}" value="${bb.untuk_id}" type="hidden"/>
                        <input id="value_from_${aa}" value="${bb.dari_id}" type="hidden"/>
                        <div class="box bg-light-info" >${bb.chat}</div>
                        <div class="chat-time" style="margin: 5px 0 15px 37px;">${addzero(dated_dated)} - ${dated_month} - ${dated_year}</div>
                    </div>
                </li>`;
                }
                
            })
                $('#isichat').append(g);
                
        })
        socket.on('baru', function(data){
            console.log(data);
                var dated = new Date(data.message.date);
                var dated_year = dated.getFullYear();
                var dated_month = dated.toLocaleString('default', { month: 'short' });
                var dated_dated = dated.getDate();
                var hh = `
                <li class="reverse">
                    <div class="chat-content">
                        <div class="box bg-light-inverse" style="background: rgba(66, 245, 93, 0.4)">${data.message.isichat}</div>
                        <div class="chat-time" style="margin: 5px 0 15px 37px;">${addzero(dated_dated)} - ${dated_month} - ${dated_year}</div>
                    </div>
                    <div class="chat-img">
                        <img src="{{asset('assets/img/man-user.png')}}" alt="user" />
                    </div>
                </li>`;
                $('#isichat').append(hh);
        })
    }
    $('#addchat').on('click',function(){
        let date = new Date();
        var message = {
            to: to[0],
            isichat: $('#isitext').val(),
            from: $('#authuser').val(),
            status: 'PERSONAL',
            date: date
        }
        socket.emit('sending message', {data: message});
        $('#isitext').val('');
    })

    $(document).on('keypress',function(e) {
        
    if(e.which == 13) {
        let a = $('#isitext').val();
        console.log({a: a, b: a.length});
        if(a !== "" && a !== "↵" && a.length > 0){
        let date = new Date();
        var message = {
            to: to[0],
            isichat: $('#isitext').val(),
            from: $('#authuser').val(),
            status: 'PERSONAL',
            date: date
        }
        socket.emit('sending message', {data: message});
        $('#isitext').val('');
        } else {
            alert('You Have to put at least 1 character');
            $('#isitext').val('');

        }
    }
});

</script>
@stop