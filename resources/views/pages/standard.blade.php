@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-5 col-sm-3 col-xs-12">
            <div class="row" id="buttonbantu" style="justify-content: center; align-items: center; flex-direction: row">
                <div style="align-self: center; flex: 1">
                    <a href="javascript:void(0)" class="btn waves-effect waves-light btn-success" onClick="addidasc()">add idasc auto</a>
                </div>
                <div style="display: flex; flex-direction: row; flex: 2;">
                    <input class="form-control" style="width: 50px; margin-right: 10px;" value="" id="idkendali"/>
                    <a href="javascript:void(0)" class="btn waves-effect waves-light btn-danger" id="updateidkendali" onClick="updatekendali()">&#10004;</a>
                </div>
            </div>
        </div>
        <div class="col-md-7 col-sm-4 col-xs-12 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
            <div class="col-md-3 col-sm-4 col-xs-12 align-self-center">
                    <select id="idpaket" style="width: 100%"></select>
            </div>
                <div class="col-md-4 col-sm-4 xs-12 align-self-center">
                    <select id="nosig" style="width: 100%"></select>
                </div>
            </div>
        </div>
    </div>
    <div id="table">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Sample Hasil</h4>
                        <div class="table-responsive m-t-40 samplehasil">
                            <table id="samplehasil"
                                class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Sample Hasil Certificate</h4>
                        <div class="table-responsive m-t-40 samplehasil">
                            <table id="samplehasilcert"
                                class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <button type="button" id="buttonupdate" class="btn waves-effect waves-light btn-success btn-lg" onClick="testing()">Update</button>
                <button type="button" id="buttonsethelper" class="btn waves-effect waves-light btn-warning btn-lg" onClick="testhelper()">Set Standart Auto</button>

            </div>
        </div>
    </div>
    <div class="row" id="preload">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="display: flex; justify-content: center; align-items: center">
                    <h4>Please select <span style="font-weight: bold;">SIG Number First</span> ..</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>   

    var o = 0;
    var idasc = false;
    var id_kont_uji = $('#nosig').val() ?  $('#nosig').val() : null;    
    var id_paket = $('#idpaket').val() ?  $('#idpaket').val() : null;    

    $(()=>{
        $('#table').css('display','none');
        $('#preload').css('display','unset');
        $('#buttonupdate').css('display','unset');
        $('#buttonbantu').css('display','none');
        select2('nosig');
        select2('idpaket');
        $('#idpaket').prop('disabled',true);
    })
    
    $('#nosig').on('change',function(){
        id_kont_uji = $(this).val();
        $.post("{{URL::to('kendalisig')}}",{id_kont_uji: id_kont_uji,status: 'get'},(c)=>{
            $('#idkendali').val(c[0].id_kendali);
        })
        samplehasil(id_kont_uji,id_paket);
    })

    $('#idpaket').on('change',function(){
        id_paket = $(this).val();
        samplehasil(id_kont_uji, id_paket);
    })

    function select2(val){
        $(`#${val}`).select2({
            minimumInputLength: val === 'nosig' ? 24 : 5,
            ajax: {
                url: `{{URL::to('${val}')}}`,
                data: function(params){
                    return {
                        q: params.term ? params.term.toUpperCase() : null,
                        page: params.page
                    }
                },
                processResults: function(data, params){
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    }
                },
                cache: true,
                delay: 2000
            },
            placeholder: val === 'nosig' ? 'SIG NO' : 'PACKET NAME'
        });
    }
    function samplehasil(v,w){
        
        $('#table').css('display','unset');
        $('#buttonupdate').css('display','unset');
        $('#preload').css('display','none');
        $('#buttonbantu').css('display','flex');
        $('#idpaket').prop('disabled',false);
        $('#samplehasil').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: true,
            paging: false,
            bDestroy: true,
            ordering: false,
            dom: 'frtip',
            ajax: {
                url: "{{URL::to('data-sample-hasil')}}",
                data: {
                    id_kont_uji: v,
                    id_paket: w ? w : null
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    width:'4%',
                    title:'No', 
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },             
                {
                    data: 'nama_parameteruji', 
                    name: 'nama_parameteruji',
                    title:'PARAMETER'
                }, 
                {
                    data: 'id_paket', 
                    name: 'id_paket',
                    title:'ID PAKET'
                },
                {
                    data: 'id_standart', 
                    name: 'id_standart',
                    title:'ID STANDART'
                },
                {
                    data: 'idasc', 
                    name: 'idasc',
                    title:'ID ASC'
                },                
            ],
            columnDefs: [
            {
                "targets": 2, // your case first column
                "className": "text-center",
                "width": "4%"
            },
            {
                "targets": 3, // your case first column
                "className": "text-center",
                "width": "4%"
            }],
        });

        $('#samplehasilcert').DataTable({
            processing: true,
            serverSide: true,
            paging: false,
            ordering: false,
            orderable: false,
            autoWidth: true,
            bDestroy: true,
            dom: 'frtip',
            ajax:{
                url: "{{URL::to('data-sample-hasil-cert')}}/",
                data: {
                    id_kont_uji: v,
                    id_paket: w ? w : null
                }
            }, 
            columns: [         
                {
                    data: null, 
                    name: 'id_standart',
                    title:'ID STANDART',
                    width: '4%',
                    render: function(data, row){
                        return `<input class="id_standard" style="width: 100px; text-align: center;" value="${data.id_standart}">
                        <input class="id_samplehasil" value="${data.id_sample_hasil}" type="hidden">`;
                    }
                },
                {
                    data: null, 
                    name: 'idasc',
                    width: '4%',
                    title:'ID ASC',
                    render: function(data, row, meta, type){
                        return (!idasc) ? `<input class="idasc" style="width: 100px; text-align: center;" value="${data.idasc}">` : `<input class="idasc" style="width: 100px" value="${type.row + type.settings._iDisplayStart + 1}">`;
                    }
                },            
            ],
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            columnDefs: [
            {
                "targets": 0, // your case first column
                "className": "text-center",
                "width": "4%"
            },
            {
                "targets": 1, // your case first column
                "className": "text-center",
                "width": "4%"
            }],
        });
        
    }
    function addidasc(){
        o++;
        if(o%2 === 1){
            idasc = true;
            samplehasil(id_kont_uji, id_paket)
        } else { 
            idasc = false;
        }
    }
    function testing(){
        var id_std = [];
        var id_sample = [];
        var idasc = [];
        var gabung = [];

        $('.id_standard').each(function(){
            id_std = id_std.concat($(this).val())
        })
        
        $('.id_samplehasil').each(function(){
            id_sample = id_sample.concat($(this).val())
        })

        $('.idasc').each(function(){
            idasc = idasc.concat($(this).val())
        })
        
        id_sample.forEach((a,b)=>{
            gabung = gabung.concat({
                id_sample: a,
                id_standart: id_std[b],
                idasc: idasc[b]
            })
        })
        $('#buttonupdate').html(`<div class="spinner-border spinner-border-sm" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>`);
        $.post("{{URL::to('data-post-save')}}",{data: gabung},function(a){
            samplehasil(id_kont_uji, id_paket);
        })
        setTimeout(() => {
            $('#buttonupdate').html(`Update`);            
        }, 2000);
    }
    function testhelper(){
        let a = `<div class="spinner-border spinner-border-md align-self-center" role="status"><span class="sr-only">Loading...</span></div>`;
        $('#buttonsethelper').html(a);
        $.post("{{URL::to('set_helper_id')}}",{data: id_kont_uji},(b)=>{
            console.log(b);
            setTimeout(() => {
                alert('Easily Done :)')
                $('#buttonsethelper').html(`Set Standart Auto`);            
            }, 3000);
        }).catch(err => alert('Ooops Something went wrong'));
        samplehasil(id_kont_uji, id_paket);
        
    }
    function updatekendali(){
        let a = `<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>`;
        $('#updateidkendali').html(a);
        $.post("{{URL::to('kendalisig')}}",{
            id_kont_uji: id_kont_uji,
            status: 'update',
            idkendali: $('#idkendali').val()
            },(c)=>{
            alert(`${c} updating id_kendali`);
            $('#updateidkendali').html(`&#10004;`);
        })
    }

</script>
@stop