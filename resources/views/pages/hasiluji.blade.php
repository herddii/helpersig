@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-3 col-sm-4 col-xs-12 align-self-center">
            <div class="align-self-center" id="buttonbantu">
                Input Data Hasil Uji
            </div>
        </div>
        <div class="col-md-9 col-sm-4 col-xs-12 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <div class="col-md-3 col-sm-3 col-xs-12 align-self-center">
                    <select id="nosig" style="width: 100%; height: 40px;"></select>
                </div>
                <div class="col-md-3 col-sm-3 xs-12 align-self-center">
                    <select id="nosample" style="width: 100%"></select>
                </div>
                <div class="col-md-3 col-sm-3 xs-12 align-self-center">
                    <select id="idpaket" style="width: 100%"></select>
                </div>
            </div>
        </div>
    </div>
    <div id="table" style="display: none;">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Data Sample Hasil</h4>
                        <div class="table-responsive m-t-40 samplehasil">
                            <table id="samplehasil"
                                class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <button type="button" id="buttonupdate" class="btn waves-effect waves-light btn-success btn-lg btn-block" onClick="upload()">Update</button>
            </div>
        </div>
    </div>
    <div class="row" id="preload">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="display: flex; justify-content: center; align-items: center">
                    <h4>Please Set <span style="font-weight: bold;">SIG Number First</span> ..</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
    var parameter = {
        id_kont_uji: $('#nosig').val() ?  $('#nosig').val() : null,
        nosample: $('#nosample').val() ?  $('#nosample').val() : null,
        id_paket: $('#idpaket').val() ?  $('#idpaket').val() : null,
    }
    
    select2('nosig');
    select2('nosample');
    select2('idpaket');
    $('#nosample').prop('disabled',true);
    $('#idpaket').prop('disabled',true);


//event
    $('#nosig').on('change',function(){
        parameter.id_kont_uji = $(this).val();
        tableshow(parameter);   
        $('#nosample').prop('disabled',false);
        $('#idpaket').prop('disabled',false);
    })

    $('#nosample').on('change',function(){
        parameter.nosample = $(this).val();
        tableshow(parameter);
    })

    $('#idpaket').on('change',function(){
        parameter.idpaket = $(this).val();
        tableshow(parameter);
    })

//function
    function select2(val){
        if(val === 'nosig'){
            var placing = 'SIG NO';
        } else if (val === 'nosample'){
            var placing = 'SAMPLE NO';
        } else {
            var placing = 'PACKET NAME';
        }
        $(`#${val}`).select2({
            minimumInputLength: val === 'nosig' ? 25 : null,
            ajax: {
                url: `{{URL::to('${val}')}}`,
                data: function(params){
                    return {
                        q: params.term ? params.term.toUpperCase() : null,
                        page: params.page,
                        id_kont_uji: parameter.id_kont_uji ? parameter.id_kont_uji : null
                    }
                },
                processResults: function(data, params){
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.last_page
                        }
                    }
                },
                cache: true,
                delay: 2000
            },
            placeholder: placing
        });
    }

    function tableshow(parameter){
        $('#table').css('display','unset');
        $('#preload').css('display','none');
        $('#samplehasil').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: true,
            paging: false,
            bDestroy: true,
            ordering: false,
            dom: 'Bfrtip',
            ajax: {
                url: "{{URL::to('hasiluji')}}",
                data: {
                    id_kont_uji: parameter.id_kont_uji,
                    nosample: parameter.nosample ? parameter.nosample : null,
                    idpaket: parameter.idpaket ? parameter.idpaket : null
                }
            },
            columns: [
                {
                    data: null,
                    sortable: false,
                    width:'4%',
                    title:'No', 
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },             
                {
                    data: 'no_iden_sam', 
                    name: 'no_iden_sam',
                    title:'SAMPLE NO'
                },
                {
                    data: 'nama_parameteruji', 
                    name: 'nama_parameteruji',
                    title:'PARAMETER'
                }, 
                {
                    data: null, 
                    name: 'hasil_uji',
                    title:'HASIL UJI',
                    render: function(data){
                        return `<input class="hasiluji form-control" value="${data.hasil_uji}"/>
                        <input class="id_samplehasil" value="${data.id_sample_hasil}" type="hidden"/>`;
                    }
                },             
            ],
        });
    }

    function upload(){
        var hasiluji = [];
        var id_samplehasil = [];
        var gabung = [];
        $('.id_samplehasil').each(function(){
            id_samplehasil = id_samplehasil.concat($(this).val());
        })

        $('.hasiluji').each(function(){
            hasiluji = hasiluji.concat($(this).val());
        })
        id_samplehasil.forEach((a,b)=>{
            gabung = gabung.concat({
                id_samplehasil: a,
                hasiluji: hasiluji[b],
            })
        })
        $('#buttonupdate').html(`<div class="spinner-border spinner-border-md" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>`);
        $.post('{{URL::to("hasiluji")}}',{data: gabung},function(a){
            tableshow(parameter);
        })
        $('#buttonupdate').html(`Update`);
    }
</script>
@stop