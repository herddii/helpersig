@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-4 col-sm-4 col-xs-12 align-self-center">
            <div class="align-self-center" id="buttonbantu">
                <h4>Testing How Fast Mongodb</h4>
            </div>
        </div>
    </div>
    <div class="row" id="preload">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="display: flex; justify-content: center; align-items: center">
                    <h4>Please select <span style="font-weight: bold;">SIG Number First</span> ..</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script>   
    $.get("{{URL::to('testing')}}",function(a){
        console.log(a);
    })
</script>
@stop