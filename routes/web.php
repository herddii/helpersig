<?php
Route::get('/',function(){
    return redirect('login');
});
Route::resource('chat','Chat\ChatController');
Route::resource('hasiluji', 'Hasiluji\HasilujiController');
Route::resource('testing', 'Test\TestController');
Route::get('standart', 'Standart\StandartController@index');
Route::get('data-sample-hasil' ,'Standart\StandartController@samplehasil');
Route::get('data-sample-hasil-cert' ,'Standart\StandartController@samplehasilcert');
Route::post('data-post-save' ,'Standart\StandartController@save_standart');
Route::post('set_helper_id', 'Standart\StandartController@sethelper');
Route::post('kendalisig' ,'Standart\StandartController@kendalisig');

//select 2
Route::get('nosig' ,'Standart\StandartController@nosig');
Route::get('idpaket' ,'Standart\StandartController@idpaket');
Route::get('nosample' ,'Hasiluji\HasilujiController@nosample');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
